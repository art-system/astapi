#ifndef MIC_EFFECTS_H_
#define MIC_EFFECTS_H_

struct mic_effect_data {
    char *name;
    char *abbriviation;
};

static struct mic_effect_data mic_effects_default[] = {
    {"Off", "OFF"},
    {"Short Room", "SHROOM"},
    {"Room", "ROOM"},
    {"Small Hall", "SMHALL"},
    {"Large Hall", "LARHALL"},
    {"Short Plate", "SHPLATE"},
    {"Vocal Plate", "VPLATE"},
    {"Church", "CHURCH"},
    {"Cathedral", "CATHEDR"},
    {"Elvin's Voice", "ELVIN"}};

#endif
