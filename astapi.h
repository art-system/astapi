/*
The MIT License (MIT)

Copyright (c) 2015, ART SYSTEM LLC.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef ASTAPI_H
#define ASTAPI_H

#define AST_EVENT_DISCONNECTED 0
#define AST_EVENT_CONNECTED 1
#define AST_EVENT_DATABASE_DOWNLOAD 2
#define AST_EVENT_DOWNLOAD_PROGRESS 3
#define AST_EVENT_DATABASE_INDEXING 4
#define AST_EVENT_ERROR 5
#define AST_EVENT_BLACKLIST 6
#define AST_EVENT_BLACKLIST_STATE 7
#define AST_EVENT_RESERV 8
#define AST_EVENT_TOP 9
#define AST_EVENT_COUNTER 10
#define AST_EVENT_PLAYER_STATE 11
#define AST_EVENT_PLAYER_STATE_LONG 12
#define AST_EVENT_BGAUDIO_STATE 13
#define AST_EVENT_MIC_EFFECT_STATE 14
#define AST_EVENT_VOLUME 15
#define AST_EVENT_CERTIFICATE 16
#define AST_EVENT_CERTIFIED_MONTHES 17
#define AST_EVENT_FW_INFO 18
#define AST_EVENT_BGVIDEO_STATE 19
#define AST_EVENT_EFFECT_LIST_CHANGED 20
#define AST_EVENT_DEVICES_LIST 21
#define AST_EVENT_RECORDINGS_LIST 22

#define AST_PLAYER_STATE_IDLE 0
#define AST_PLAYER_STATE_PLAYING 1
#define AST_PLAYER_STATE_PAUSED 2

#define AST_BGAUDIO_STATE_IDLE 0
#define AST_BGAUDIO_STATE_PLAYING 1
#define AST_BGAUDIO_STATE_PAUSED 2

#define AST_ERROR_NETWORK_SEND -100
#define AST_ERROR_NETWORK_INIT -101
#define AST_ERROR_CONNECT -102
#define AST_ERROR_DATABASE_INFO -103
#define AST_ERROR_DATABASE_SYNC -104
#define AST_ERROR_DATABASE_DOWNLOAD -105
#define AST_ERROR_CERTIFICATE_INFO -106
#define AST_ERROR_INCOMPATIBLE_FIRMWARE_VERSION -107
#define AST_ERROR_FW_INFO -108
#define AST_ERROR_TOO_MODERN_FIRMWARE -109

#define AST_RUNTIME_ERROR_UNKNOWN -110
#define AST_RUNTIME_ERROR_NON_KARAOKE_MODE -111
#define AST_RUNTIME_ERROR_PIN_NOT_ACCEPTED -112
#define AST_RUNTIME_ERROR_BROADCAST_FAILED -113
#define AST_RUNTIME_ERROR_INVALID_REQUEST -114

#define MINIMUM_TEMPO_VALUE -7
#define MAXIMUM_TEMPO_VALUE 7
#define TEMPO_STEP_PERCENT 4
#define MINIMUM_KEY_VALUE -6
#define MAXIMUM_KEY_VALUE 6
#define MINIMUM_MELODY_VALUE 0
#define MAXIMUM_MELODY_VALUE 3
#define MINIMUM_VOLUME_VALUE 0
#define MAXIMUM_VOLUME_VALUE 100

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#define TCHAR wchar_t
#else
#define TCHAR char
#endif

struct ast_fw_info {
    int version_major;
    int version_minor;
    int build_number;
    char serial[13];
    char board_id[13];
};

enum ast_api_features { volume_v2 = 1, effect_list = 2, pin_code_enabled = 4 };

struct ast_db_info {
    int db_release;
    int db_crc;
    int db_size;
};

enum ast_line_count {
    ast_line_count_default,
    ast_line_count_2,
    ast_line_count_4
};

#define AST_SONG_ENTRY_ID_MAX_LEN 36 // the UUID string representation length

struct ast_song_parameters_ext {
    int line_count; // ast_line_count_xxx
    int vip_song;
    char entry_id[AST_SONG_ENTRY_ID_MAX_LEN + 1]; // var-len, zero-terminated
};

#define AST_SONG_UID_SIZE 32

struct ast_song_parameters {
    int num;
    int key;
    int tempo;
    int melody;
    struct ast_song_parameters_ext ext;
    unsigned char uid[AST_SONG_UID_SIZE];
};

enum reserve_modify_option {
    reserve_modify_option_key,
    reserve_modify_option_tempo,
    reserve_modify_option_melody,
    reserve_modify_option_line_count,
    reserve_modify_option_number
};

struct ast_event_progress {
    int current;
    int total;
};

struct ast_event_song_list {
    int num_songs;
    int songs[1];
};

struct ast_event_player_state {
    int state;
    int position;
    int duration;
    struct ast_song_parameters params;
};

struct ast_event_player_state_long {
    int state;
    int song_num;
    char *title;
    char *artist;
};

struct ast_event_reserv {
    int num_songs;
    struct ast_song_parameters songs[1];
};

struct ast_event_mic_effect_state {
    char mic_effect[10];
    int mic_volume;
    int mic_level;
    int mic_volume_2;
};

enum ast_cert_credentials { ast_cert_cdg = 1, ast_cert_tcp = 2 };

struct month_interval {
    int start;
    int end;
};

struct ast_event_certified_dates {
    int num_intervals;
    struct month_interval intervals[1];
};

enum ast_bga_state {
    ast_bgastate_disabled,
    ast_bgastate_idle,
    ast_bgastate_playing,
    ast_bgastate_paused
};

enum ast_bgv_state {
    ast_bgvstate_disabled,
    ast_bgvstate_idle,
    ast_bgvstate_playing,
};

enum ast_bgv_mode {
    ast_bgvmode_background,
    ast_bgvmode_capture,
    ast_bgvmode_clip,
};

struct ast_event_bgastate {
    enum ast_bga_state state;
    int position;
    int duration;
    const char *url;
};

struct ast_event_bgvstate {
    enum ast_bgv_state state;
    enum ast_bgv_mode mode;
    int position;
};

struct ast_device_info {
    char ip[16];
    char sn[13];
    int model;
};

#define AST_DEVICE_LIST_LIMIT 10
struct ast_devices_list {
    int count;
    struct ast_device_info dev[AST_DEVICE_LIST_LIMIT];
};

struct ast_recording_info {
    int id, date, song, key, tempo, melody;
    const char *title, *singer;
};

struct ast_recordings_list {
    unsigned count;
    struct ast_recording_info rec[1];
};

struct ast_event {
    int event_type;
    struct ast_event *event_next;
    union {
        int error;
        int counter;
        int bgaudio_state;
        struct ast_event_progress download_progress;
        struct ast_event_progress indexing_progress;
        struct ast_fw_info fw_info;
        struct ast_event_player_state player_state;
        struct ast_event_player_state_long player_state_long;
        struct ast_event_song_list blacklist;
        struct ast_event_song_list top_songs;
        struct ast_event_reserv reserv;
        struct ast_event_mic_effect_state effect;
        enum ast_cert_credentials certificate;
        struct ast_event_certified_dates dates;
        struct ast_event_bgastate bgastate;
        struct ast_event_bgvstate bgvstate;
        struct ast_devices_list devices;
        struct ast_recordings_list recordings;
        int volume;
        int blacklist_enabled;
    } event_data;
};

enum ast_api_state {
    ast_api_offline,
    ast_api_discovering,
    ast_api_connecting,
    ast_api_downloading,
    ast_api_online,
    ast_api_error
};

struct ast_api;

int ast_api_init(const TCHAR *db_path, const TCHAR *db_crc_path,
                 const TCHAR *alt_path, const TCHAR *alt_crc_path);

int ast_api_init_v(const TCHAR *db_path, const TCHAR *db_crc_path,
                   const TCHAR *alt_path, const TCHAR *alt_crc_path,
                   struct ast_fw_info minimumVersion,
                   struct ast_fw_info maximumVersion);

int ast_api_free();

int ast_scan();

int ast_cancel_scan();

int ast_connect(const char *hostname);

int ast_disconnect();

enum ast_api_state ast_state();

int ast_features();

int ast_event_pull(struct ast_event **ppevent);

int ast_event_free(struct ast_event **ppevent);

int ast_ask_player_state_long();

int ast_play(const struct ast_song_parameters *params);

int ast_play_next();

int ast_pause();

int ast_stop();

int ast_reserve(const struct ast_song_parameters *params);

int ast_priority_reserve(const struct ast_song_parameters *params);

int ast_reserve_move(int pos, int to);

int ast_reserve_delete(int pos);

int ast_reserve_clear();

int ast_reserve_pause(int value);

int ast_reserve_modify(const struct ast_song_parameters *params, int position,
                       enum reserve_modify_option option);

int ast_reserve_set(const struct ast_song_parameters *songs, int num_songs);

int ast_blacklist_state();

int ast_blacklist_fetch();

int ast_bgvideo_switch_mode();

int ast_bgvideo_play(const char *url);

int ast_bgaudio_play(const char *url);

int ast_bgaudio_applause();

int ast_bgaudio_stop();

int ast_bgaudio_pause();

int ast_bgaudio_resume();

int ast_bgaudio_set_volume(int value);

int ast_recording_start_replay(int id);

int ast_recording_stop_replay();

int ast_recording_remove(int id);

int ast_recordings_clear();

int ast_songcounter();

int ast_toplist();

int ast_fwinfo();

int ast_ask_certificate();

enum ast_sound_command {
    ast_mic_effect_up,
    ast_mic_effect_down,
    ast_mic_level_up,
    ast_mic_level_down,
    ast_mic_volume_up,
    ast_mic_volume_down,
    ast_tone_up,
    ast_tone_down,
    ast_tempo_up,
    ast_tempo_down,
    ast_melody_up,
    ast_melody_down,
    ast_volume_up,
    ast_volume_down
};

int ast_apply_sound_command(enum ast_sound_command command);

enum ast_sound_parameter {
    ast_sound_parameter_mic_volume,
    ast_sound_parameter_mic_effect,
    ast_sound_parameter_mic_effect_level,
    ast_sound_parameter_volume,
    ast_sound_parameter_tempo,
    ast_sound_parameter_key,
    ast_sound_parameter_melody,
    ast_sound_parameter_mic_volume_2,
    // for internal usage only
    ast_sound_parameter_lowband_gain,
    ast_sound_parameter_midband_gain,
    ast_sound_parameter_highband_gain
};

const char *ast_hostname();
// May return NULL
const char *ast_clientname();

int ast_mic_effect_count();
const char *ast_mic_effect_string(int value);
const char *ast_mic_effect_abbriviation(int value);
const char *ast_mic_effect_string_for_abbriviation(char *abbr);

int ast_apply_sound_parameter_value(enum ast_sound_parameter param, int value);

int ast_set_pin(int pincode);

int ast_parse_boardid(const char *s);

// These functions accept base value for a paramater and a
// parameter change in steps and produce formatted
// representation of those values. In the case of successfull call,
// caller is responsible for freeing the return buffer.
int ast_format_key(int voice, int key, int key_change, char **res);
int ast_format_tempo(int tempo, int tempo_change, char **res);
int ast_format_melody(int type, int melody, char **res);

int ast_format_boardid(int board_id, char **res);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ASTAPI_H
