#include "server.h"
#include <mongoose.h>

int main()
{
    struct mg_mgr mgr_udp;
    struct mg_mgr mgr_tcp;

    struct ServerData server          = {.db_path  = "content.db",
                                .alt_path = "alternate.txt"};
    server.player_state.volume        = 80;
    server.mic_state.volume           = 0;
    server.mic_state.mic_effect_level = 60;
    strcpy(server.mic_state.mic_effect, "OFF");
    reserve_alloc(&server.reserve_state);

    mg_mgr_init(&mgr_udp, NULL);
    mg_mgr_init(&mgr_tcp, &server);

    mg_bind(&mgr_udp, "udp://0.0.0.0:2201", event_handler_udp);
    struct mg_connection *c =
        mg_bind(&mgr_tcp, "tcp://0.0.0.0:2202", event_handler_tcp);

    mg_set_timer(c, next_timer_time());

    for (;;) {
        mg_mgr_poll(&mgr_udp, 1);
        mg_mgr_poll(&mgr_tcp, 1);
    }

    mg_mgr_free(&mgr_udp);
    mg_mgr_free(&mgr_tcp);
    free(server.reserve_state);
    return 0;
}
