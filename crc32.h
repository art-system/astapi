#pragma once

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#define TCHAR wchar_t
#else
#define TCHAR char
#endif

uint32_t crc32_calc_from_stream(FILE *file);
uint32_t crc32_of_file(const TCHAR *filename);

#ifdef __cplusplus
}//extern "C"
#endif
