# Makefile for building test app on your Desktop Computer. 
# Makefile assumes that you have libuv1-dev installed and sds lib is lying in the sibling folder
CFLAGS=-DUV_VERSION_MAJOR=1
main: main.c astapi.c astapi.h
	gcc main.c astapi.c crc32.c ../sds/sds.c -I../sds/ -luv -lm -o main -D_DEBUG -g

server: main-server.c server.c server.h ../mongoose/mongoose.c crc32.c
	gcc main-server.c server.c crc32.c ../mongoose/mongoose.c ../sds/sds.c -I../mongoose/ -I../sds/ -lm -o server -g

sanitize: main.c astapi.c astapi.h
	gcc -O2 -fsanitize=address -fsanitize=leak -fsanitize=undefined main.c astapi.c crc32.c ../sds/sds.c -I../sds/ -I"$(HOME)/Downloads/libuv-v1.18.0/include" -L"$(HOME)/Downloads/libuv-v1.18.0/out/Debug" -luv -lm -o main -lrt -lpthread -D_DEBUG -g

downloads:
	wget --no-check-certificate http://www.astupdate.com/astdb_v2/content.zip \
		&& unzip content.zip \
		&& wget --no-check-certificate http://www.astupdate.com/astdb_v2/alternate.zip \
		&& unzip alternate.zip

.PHONY: clean
clean:
	rm main

cleancontent:
	-rm content.zip content.db alternate.zip alternate.txt
