/*
The MIT License (MIT)

Copyright (c) 2015, ART SYSTEM LLC.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "astapi.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>
#include <uv.h>

int application_running = 1;

uv_loop_t *loop;
uv_buf_t default_alloc(uv_handle_t *handle, size_t suggested_size);
void handle_events();

int inputAvailable()
{
    struct timeval tv;
    fd_set fds;
    tv.tv_sec  = 0;
    tv.tv_usec = 10;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
    return (FD_ISSET(0, &fds));
}

void signal_handler(uv_signal_t *handle, int signum)
{
    printf("Signal received: %d\n", signum);
    application_running = 0;
    uv_signal_stop(handle);
}

int main(int argc, char **argv)
{
    const char *default_host = "";
    if (argc > 1)
        default_host = argv[1];

    loop = uv_loop_new();

    ast_api_init(":skip:", ":skip:", ":skip:", ":skip:");
    if (strlen(default_host) != 0) {
        printf("connecting to %s ...", default_host);
        ast_connect(default_host);
    } else {
        ast_scan();
    }

    char cmd[10];
    char val[10];

    while (application_running) {
        handle_events();
        if (inputAvailable() && scanf("%s %s", cmd, val) >= 1) {
            if (strncmp(cmd, "bgv", 4) == 0) {
                ast_bgvideo_switch_mode();
            } else if (strncmp(cmd, "pin", 3) == 0) {
                ast_set_pin(atoi(val));
            } else if (strncmp(cmd, "vol", 3) == 0) {
                ast_apply_sound_parameter_value(ast_sound_parameter_volume,
                                                atoi(val));
            }
        }
    }

    ast_api_free();
    return 0;
}

void handle_events()
{
    struct ast_event *e;
    while (ast_event_pull(&e)) {
        fprintf(stderr, "event %d\n", e->event_type);
        switch (e->event_type) {
        case AST_EVENT_CONNECTED:
            fprintf(stderr, "CONNECTED\n");
            fprintf(stderr, "effect count = %d\n", ast_mic_effect_count());
            for (int i = 0; i < ast_mic_effect_count(); i++) {
                fprintf(stderr, "effect: %s\n", ast_mic_effect_string(i));
            }
            fprintf(stderr, "features = %d\n", ast_features());
            break;
        case AST_EVENT_DISCONNECTED:
            fprintf(stderr, "DISCONNECTED\n");
            break;
        case AST_EVENT_ERROR:
            fprintf(stderr, "error %d\n", e->event_data.error);
            break;
        case AST_EVENT_DEVICES_LIST: {
            int count = e->event_data.devices.count;
            if (count > 0) {
                fprintf(stderr, "devices count %d\n", count);
                for (int i = 0; i < count; ++i) {
                    struct ast_device_info *dev = &e->event_data.devices.dev[i];
                    fprintf(stderr, "device %d: ip %s, sn %s, model %d\n", i,
                            dev->ip, dev->sn, dev->model);
                }
                int index = count > 1 ? -1 : 0;
                while (index < 0 || index >= count) {
                    printf("Enter valid device index to connect: ");
                    if (!scanf("%d", &index)) {
                        continue;
                    }
                }
                ast_connect(e->event_data.devices.dev[index].ip);
            } else {
                fprintf(stderr, "no devices found\n");
            }
            break;
        }
        case AST_EVENT_BLACKLIST:
            break;
        case AST_EVENT_RESERV:
            fprintf(stderr, "reserve list: %d\n",
                    e->event_data.reserv.num_songs);
            break;
        case AST_EVENT_TOP:
            break;
        case AST_EVENT_COUNTER:
            break;
        case AST_EVENT_PLAYER_STATE: {
            struct ast_event_player_state *ps = &e->event_data.player_state;
            fprintf(stderr, "player state: %d %d/%d %32s\n", ps->state,
                    ps->position, ps->duration, ps->params.uid);
            break;
        }
        case AST_EVENT_BGAUDIO_STATE:
            break;
        case AST_EVENT_FW_INFO:
            fprintf(stderr, "board id: %s\n", e->event_data.fw_info.board_id);
            break;
        case AST_EVENT_CERTIFICATE:
            fprintf(stderr, "certificate : %d\n", e->event_data.certificate);
            break;
        case AST_EVENT_CERTIFIED_MONTHES:
            fprintf(stderr, "certified months : %d\n",
                    e->event_data.dates.num_intervals);
            for (int i = 0; i < e->event_data.dates.num_intervals; ++i) {
                fprintf(stderr, "certified months : %d - %d\n",
                        e->event_data.dates.intervals[i].start,
                        e->event_data.dates.intervals[i].end);
            }
            break;
        case AST_EVENT_VOLUME:
            fprintf(stderr, "Volume = %d\n", e->event_data.volume);
            break;
        case AST_EVENT_MIC_EFFECT_STATE:
            fprintf(stderr,
                    "New mic effect state:\n level = %d\n volume = %d\n "
                    "volume2 = %d\n",
                    e->event_data.effect.mic_level,
                    e->event_data.effect.mic_volume,
                    e->event_data.effect.mic_volume_2);
            break;
        case AST_EVENT_BGVIDEO_STATE:
            switch (e->event_data.bgvstate.state) {
            case ast_bgvstate_playing:
                fprintf(stderr, "bgv playing\n");
                break;
            case ast_bgvstate_idle:
                fprintf(stderr, "bgv idle\n");
                break;
            case ast_bgvstate_disabled:
                fprintf(stderr, "bgv disabled\n");
                break;
            }
            switch (e->event_data.bgvstate.mode) {
            case ast_bgvmode_background:
                fprintf(stderr, "bgv background\n");
                break;
            case ast_bgvmode_clip:
                fprintf(stderr, "bgv clip\n");
                break;
            case ast_bgvmode_capture:
                fprintf(stderr, "bgv capture\n");
                break;
            }
            break;
        case AST_EVENT_RECORDINGS_LIST: {
            int count = e->event_data.recordings.count;
            printf("recordings count: %d\n", count);
            struct ast_recording_info *info = e->event_data.recordings.rec;
            for (int i = 0; i < count; ++i) {
                time_t t = (time_t)info[i].date;
                printf("uid %d, song %d, key: %d, tempo: %d, melody: %d, "
                       "title: %s, singer: %s, time: %s",
                       info[i].id, info[i].song, info[i].key, info[i].tempo,
                       info[i].melody, info[i].title, info[i].singer,
                       ctime(&t));
            }
            break;
        }
        }
        ast_event_free(&e);
    }
}

uv_buf_t default_alloc(uv_handle_t *handle, size_t suggested_size)
{
    uv_buf_t buf;
    buf.base = malloc(suggested_size);
    buf.len  = suggested_size;
    return buf;
}
