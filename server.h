#ifndef ASTAPI_SERVER_H_
#define ASTAPI_SERVER_H_

#include "mongoose.h"

enum PlayerPlayState { PlayerIdle, PlayerPaused, PlayerRunning };

struct PlayerState {
    enum PlayerPlayState state;
    uint32_t num;
    int8_t tone;
    int8_t tempo;
    int8_t melody;
    int8_t volume;
    uint8_t position;
    uint8_t duration;
};

struct ReserveItem {
    uint32_t num;
    int8_t tone;
    int8_t tempo;
    int8_t melody;
};

struct Reserve {
    struct ReserveItem items[20];
    uint8_t length;
};

struct MicState {
    int8_t volume;
    int8_t mic_effect_level;
    char mic_effect[10];
};

struct ServerData {
    const char *db_path;
    const char *alt_path;

    struct PlayerState player_state;
    struct Reserve *reserve_state;
    struct MicState mic_state;

    bool reserve_changed;
    bool mic_effect_changed;
    bool player_effect_changed;
};

struct ClientState {
    bool subscribed;
};

void event_handler_udp(struct mg_connection *nc, int ev, void *ev_data);
void event_handler_tcp(struct mg_connection *nc, int ev, void *ev_data);

double next_timer_time();

int reserve_alloc(struct Reserve **reserve);

#endif
