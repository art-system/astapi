/*
The MIT License (MIT)

Copyright (c) 2015, ART SYSTEM LLC.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdlib.h>

#include "astapi.h"
#include "crc32.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <math.h>
#include <memory.h>
#include <sds.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <uv.h>

#ifdef _WIN32
#include <malloc.h>
#else
#include <alloca.h>
#endif

#ifdef _WIN32
#define snprintf _snprintf
#define strdup _strdup
#define strcmpi _stricmp
#define strncmpi _strnicmp
#else
#define strcmpi strcasecmp
#define strncmpi strncasecmp
#endif

#ifndef min
static int min(int first, int second)
{
    return first < second ? first : second;
}
#endif

#ifndef max
static int max(int first, int second)
{
    return first > second ? first : second;
}
#endif

#ifdef __ANDROID__
#include <android/log.h>
#define DBG_PRINTF(fmt, ...)                                                   \
    __android_log_print(ANDROID_LOG_INFO, "native", fmt "\n", ##__VA_ARGS__)
#else
#define DBG_PRINTF(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#endif

#ifdef _DEBUG
#define DBG DBG_PRINTF
#else
#define DBG(fmt, ...) (void)0
#endif

#include "miceffects.h"

struct discovery_state {
    int last_time_broadcasts_sent;
    int last_time_broadcasts_failed;
};

struct ast_api {
    enum ast_api_state state;
    uv_loop_t *uvloop;
    uv_tcp_t *tcp;
    uv_udp_t *udp;
    uv_timer_t reconnect_timer;
    uv_timer_t keep_alive_timer;
    uv_timer_t scan_timer;
    uv_connect_t tcp_connect;
    uv_udp_send_t code_info_req;

    sds large_buffer;

    ssize_t dbsync_progress;
    ssize_t dbsync_total;
    // Address of device we connect to
    char *hostname;
    // Address of the client which connects to the device
    char *clientname;
    struct ast_event *event_list;
    TCHAR *db_path;
    TCHAR *alter_path;

    uint32_t features;
    ssize_t effect_count;
    struct mic_effect_data *effect_list;
    struct ast_devices_list device_list;
    struct discovery_state discovery_state;
};

static const int KEEP_ALIVE_TIME = 10000;
static const int SCAN_TIME       = 1000;
static time_t g_last_message_time;

static struct ast_api *api;

static struct ast_fw_info *g_minimumVersion;
static struct ast_fw_info *g_maximumVersion;

static FILE *g_dbFile;
static uint32_t g_defferCRC;

void add_to_event_list(struct ast_event *e);
struct ast_event *ast_event_new(int event_type);
void ast_event_delete(struct ast_event *e);
static void on_connect(uv_connect_t *req, int status);

static void on_info_read(uv_udp_t *handle, ssize_t nread, const uv_buf_t *buf,
                         const struct sockaddr *addr, unsigned flags);
static int send_msg(char *s);
static void state_offline();
static int send_request(const char *request_str, uv_stream_t *stream,
                        uv_write_cb reader_cb);
static void sync_database();
static void start_reconnect_timer(uint64_t t);
static void start_scan_timer(uint64_t t);
void subscribe();
void unsubscribe();
static void state_error(int err);
static int udp_init();
static int udp_connect();

static void sync_alter();

static int is_scanning();

static void on_fw_info_response(char *message_text);
static void on_certinfo_response(char *message_text);
static void on_features_response(char *message_text);
static void on_effect_list_response(char *message_text);
static void on_dbinfo_response(char *message_text);
static void on_alterinfo_response(char *message_text);

static void on_reconnect_timeout(uv_timer_t *handle);

static void on_udp_bcast_sent(uv_udp_send_t *req, int status);

void astapi_custom_alloc(uv_handle_t *handle, size_t size, uv_buf_t *buf)
{
    buf->base = malloc(size);
    buf->len  = size;
}

int ast_api_init(const TCHAR *db_path, const TCHAR *db_crc_path,
                 const TCHAR *alt_path, const TCHAR *alt_crc_path)
{
    if (api)
        return 0;
    api             = (struct ast_api *)calloc(1, sizeof(struct ast_api));
    uv_loop_t *loop = malloc(sizeof *loop);
    api->uvloop     = loop;
    uv_loop_init(loop);
    api->state      = ast_api_offline;
    api->event_list = 0;
    uv_timer_init(api->uvloop, &api->reconnect_timer);
    uv_timer_init(api->uvloop, &api->keep_alive_timer);
    uv_timer_init(api->uvloop, &api->scan_timer);

#ifdef _WIN32
    api->db_path    = _wcsdup(db_path);
    api->alter_path = _wcsdup(alt_path);
#else
    api->db_path    = strdup(db_path);
    api->alter_path = strdup(alt_path);
#endif

    return 0;
}

int ast_api_init_v(const TCHAR *db_path, const TCHAR *db_crc_path,
                   const TCHAR *alt_path, const TCHAR *alt_crc_path,
                   struct ast_fw_info minimumVersion,
                   struct ast_fw_info maximumVersion)
{
    int err = ast_api_init(db_path, db_crc_path, alt_path, alt_crc_path);
    if (err)
        return err;

    if (g_minimumVersion != 0) {
        free(g_minimumVersion);
    }
    g_minimumVersion =
        (struct ast_fw_info *)calloc(1, sizeof(struct ast_fw_info));
    g_minimumVersion->version_major = minimumVersion.version_major;
    g_minimumVersion->version_minor = minimumVersion.version_minor;
    g_minimumVersion->build_number  = minimumVersion.build_number;

    if (g_maximumVersion != 0) {
        free(g_maximumVersion);
    }
    g_maximumVersion =
        (struct ast_fw_info *)calloc(1, sizeof(struct ast_fw_info));
    g_maximumVersion->version_major = maximumVersion.version_major;
    g_maximumVersion->version_minor = maximumVersion.version_minor;
    g_maximumVersion->build_number  = maximumVersion.build_number;
    return 0;
}

int ast_features()
{
    if (api == 0) {
        return 0;
    }
    return api->features;
}

int ast_api_free()
{
    if (api) {
        ast_disconnect();
        uv_loop_close(api->uvloop);
        free(api->uvloop);
        sdsfree(api->large_buffer);
        free(api->db_path);
        free(api->alter_path);
        free(api->hostname);
        free(api->clientname);
        free(api);
        api = 0;
    }

    if (g_minimumVersion != 0) {
        free(g_minimumVersion);
        g_minimumVersion = 0;
    }
    if (g_maximumVersion != 0) {
        free(g_maximumVersion);
        g_maximumVersion = 0;
    }
    return 0;
}

static int is_scanning()
{
    return api->state == ast_api_discovering &&
           (!api->hostname || strlen(api->hostname) == 0);
}

static int send_info_request()
{

    DBG("send discovery request");
    static uint32_t args[3] = {0xAAACAAAB, 0x00000000, 0x00000000};
    uv_buf_t buf            = uv_buf_init((char *)&args, sizeof(args));

    int err;
    if (!is_scanning()) {
        struct sockaddr_in send_addr;
        err = uv_ip4_addr(api->hostname, 2201, &send_addr);
        if (err) {
            DBG("uv_ip4_addr error code=%d", err);
            return err;
        }

        err = uv_udp_send(&api->code_info_req, api->udp, &buf, 1,
                          (const struct sockaddr *)&send_addr, 0);
        if (err) {
            DBG("uv_udp_send error code=%d", err);
        }
        return err;
    }

    struct sockaddr_in send_addr;
    (void)uv_ip4_addr("239.0.0.1", 2201, &send_addr);

    err = uv_udp_send(&api->code_info_req, api->udp, &buf, 1,
                      (const struct sockaddr *)&send_addr, 0);
    if (err) {
        DBG("uv_udp_send error code=%d", err);
        return err;
    }

    (void)uv_run(api->uvloop, UV_RUN_NOWAIT);

    err = uv_udp_set_broadcast(api->udp, 1);
    if (err) {
        DBG("uv_udp_set_broadcast error code=%d", err);
        return err;
    }

    struct sockaddr_in broadcast_send_addr;
    (void)uv_ip4_addr("255.255.255.255", 2201, &broadcast_send_addr);

    api->discovery_state.last_time_broadcasts_failed = 0;
    api->discovery_state.last_time_broadcasts_sent   = 0;

    err = uv_udp_send(&api->code_info_req, api->udp, &buf, 1,
                      (const struct sockaddr *)&broadcast_send_addr,
                      on_udp_bcast_sent);
    if (err) {
        DBG("uv_udp_send error code=%d", err);
        return err;
    } else {
        api->discovery_state.last_time_broadcasts_sent += 1;
    }

    (void)uv_run(api->uvloop, UV_RUN_NOWAIT);

    // local request
    int addrlist_count               = 0;
    uv_interface_address_t *addrlist = 0;

    err = uv_interface_addresses(&addrlist, &addrlist_count);
    if (err) {
        DBG("uv_interface_addresses error code=%d", err);
        // Libc-based retrieval of interfaces fails on Android 11+.
        // Android-specific (Java SDK) facilities should be used instead.
        // This is beyond our scope. Permit the call to fail.
        // Interrupting at this produces acceptable results.
#ifdef __ANDROID__
        return 0;
#else
        return err;
#endif
    }

    DBG("addrlist_count %d", addrlist_count);

    for (int i = 0; i < addrlist_count; ++i) {
        uv_interface_address_t *ifaddr = &addrlist[i];

        if (ifaddr->is_internal ||
            ifaddr->address.address4.sin_family != AF_INET) {
            continue;
        }

        struct sockaddr_in dst_addr = ifaddr->address.address4;
        dst_addr.sin_addr.s_addr |= ~ifaddr->netmask.netmask4.sin_addr.s_addr;

        if ((err = uv_udp_send(&api->code_info_req, api->udp, &buf, 1,
                               (struct sockaddr *)&dst_addr,
                               on_udp_bcast_sent))) {
            DBG("uv_udp_send error code=%d", err);
            return err;
        } else {
            api->discovery_state.last_time_broadcasts_sent += 1;
        }

        (void)uv_run(api->uvloop, UV_RUN_NOWAIT);
    }

    (void)uv_free_interface_addresses(addrlist, addrlist_count);
    return 0;
}

static void start_timer(uv_timer_t *timer, void (*on_timeout)(uv_timer_t *),
                        uint64_t timeout, uint64_t repeat)
{
    uv_update_time(api->uvloop);
    uv_timer_start(timer, on_timeout, timeout, repeat);
}

static void start_reconnect_timer(uint64_t timeout)
{
    DBG("start_reconnect_timer start %u\n", (unsigned)timeout);
    start_timer(&api->reconnect_timer, on_reconnect_timeout, timeout, 0);
}

static void on_reconnect_timeout(uv_timer_t *handle)
{
    DBG("on_reconnect_timeout %p", handle);

    if (api->udp) {
        uv_udp_recv_stop(api->udp);
    }

    udp_connect();

    start_reconnect_timer(5000);
}

static void stop_reconnect_timer()
{
    DBG("stop_reconnect_timer");
    uv_timer_stop(&api->reconnect_timer);
}

struct ast_event *ast_event_new(int event_type)
{
    struct ast_event *e = calloc(1, sizeof(struct ast_event));
    if (!e)
        return NULL;
    e->event_type = event_type;
    e->event_next = 0;
    return e;
}

struct ast_event *ast_reserv_event_new(int reserve_size)
{
    size_t memory_size = sizeof(struct ast_event) +
                         sizeof(struct ast_song_parameters) * reserve_size;
    struct ast_event *e = calloc(1, memory_size);
    e->event_type       = AST_EVENT_RESERV;
    e->event_next       = 0;

    e->event_data.reserv.num_songs = reserve_size;

    return e;
}

struct ast_event *ast_toplist_event_new(size_t song_count)
{
    size_t memory_size = sizeof(struct ast_event) +
                         sizeof(struct ast_event_song_list) +
                         sizeof(int) * song_count;
    struct ast_event *e = calloc(1, memory_size);
    if (!e) {
        return NULL;
    }
    e->event_type = AST_EVENT_TOP;
    e->event_next = 0;

    e->event_data.reserv.num_songs = song_count;

    return e;
}

struct ast_event *ast_player_state_long_event_new(int state, int num,
                                                  char *title, int titleLen,
                                                  char *artist, int artistLen)
{
    size_t memory_size = sizeof(struct ast_event) +
                         sizeof(char) * ((titleLen + 1) + (artistLen + 1));
    struct ast_event *e                   = calloc(1, memory_size);
    e->event_type                         = AST_EVENT_PLAYER_STATE_LONG;
    e->event_data.player_state_long.state = state;
    char *ptr                             = (char *)e;
    ptr += sizeof(struct ast_event);
    e->event_data.player_state_long.song_num = num;
    e->event_data.player_state_long.title    = strncpy(ptr, title, titleLen);
    ptr += titleLen + 1;
    e->event_data.player_state_long.artist = strncpy(ptr, artist, artistLen);
    e->event_next                          = 0;

    return e;
}

struct ast_event *ast_bgastate_event_new(enum ast_bga_state state,
                                         const char *url, size_t url_len,
                                         int position, int duration)
{
    size_t memory_size =
        sizeof(struct ast_event) + sizeof(char) * (url_len + 1);
    struct ast_event *e             = calloc(1, memory_size);
    e->event_type                   = AST_EVENT_BGAUDIO_STATE;
    e->event_data.bgastate.state    = state;
    e->event_data.bgastate.position = position;
    e->event_data.bgastate.duration = duration;

    char *ptr = (char *)e;
    ptr += sizeof(struct ast_event);
    e->event_data.bgastate.url = strncpy(ptr, url, url_len);
    e->event_next              = 0;

    return e;
}

struct ast_event *ast_bgvstate_event_new(enum ast_bgv_state state,
                                         enum ast_bgv_mode mode, int position)
{
    size_t memory_size = sizeof(struct ast_event) + sizeof(state) +
                         sizeof(mode) + sizeof(position);
    struct ast_event *e             = calloc(1, memory_size);
    e->event_type                   = AST_EVENT_BGVIDEO_STATE;
    e->event_data.bgvstate.state    = state;
    e->event_data.bgvstate.mode     = mode;
    e->event_data.bgvstate.position = position;

    e->event_next = 0;

    return e;
}

struct ast_event *ast_deviceslist_event_new(struct ast_devices_list *lst)
{
    size_t memory_size =
        sizeof(struct ast_event) + sizeof(struct ast_devices_list);
    struct ast_event *e = calloc(1, memory_size);
    e->event_type       = AST_EVENT_DEVICES_LIST;
    e->event_next       = NULL;
    memcpy(&e->event_data.devices, lst, sizeof(struct ast_devices_list));
    return e;
}

struct ast_event *ast_recordingslist_event_new(unsigned recordings_count,
                                               unsigned str_pool_size,
                                               char **str_pool)
{
    size_t memory_size =
        sizeof(struct ast_event) + sizeof(struct ast_recordings_list) +
        sizeof(struct ast_recording_info) * recordings_count + str_pool_size;
    struct ast_event *e = calloc(1, memory_size);
    if (e) {
        *str_pool     = (char *)e + memory_size - str_pool_size;
        e->event_type = AST_EVENT_RECORDINGS_LIST;
        e->event_next = NULL;
        e->event_data.recordings.count = recordings_count;
    }
    return e;
}

void ast_event_delete(struct ast_event *e)
{
    free(e);
}

void add_to_event_list(struct ast_event *e)
{
    struct ast_event *i = api->event_list;

    while (i && i->event_next)
        i = i->event_next;

    if (!i)
        api->event_list = e;
    else
        i->event_next = e;
}

static void process_player_state_message(char *message_text)
{
    char state[13]                  = {};
    char uid[AST_SONG_UID_SIZE + 2] = {};
    int songId                      = -1;
    int position                    = -1;
    int duration                    = -1;

    int tone   = 0;
    int temp   = 0;
    int melody = 0;

    if (sscanf(message_text, "%*s %12s %d %d %d %d %32s %d %d", state, &songId,
               &tone, &temp, &melody, uid, &position, &duration)) {
        struct ast_event *e = ast_event_new(AST_EVENT_PLAYER_STATE);
        if (!e)
            return;

        if (strncmp("running", state, 7) == 0) {
            e->event_data.player_state.state = AST_PLAYER_STATE_PLAYING;
        } else if (strncmp("paused", state, 6) == 0) {
            e->event_data.player_state.state = AST_PLAYER_STATE_PAUSED;
        } else {
            e->event_data.player_state.state = AST_PLAYER_STATE_IDLE;
        }
        memcpy(e->event_data.player_state.params.uid, uid, AST_SONG_UID_SIZE);
        e->event_data.player_state.duration      = duration;
        e->event_data.player_state.position      = position;
        e->event_data.player_state.params.num    = songId;
        e->event_data.player_state.params.key    = tone;
        e->event_data.player_state.params.tempo  = temp;
        e->event_data.player_state.params.melody = melody;

        add_to_event_list(e);
    }
}

static void process_player_state_long_message(char *message_text)
{
#define BUFFER_LEN 64

    char strings[6][BUFFER_LEN];

    strtok(message_text, "\t");
    int i = 0;

    char *buf;

    while ((buf = strtok(NULL, "\t"))) {
        memset(strings[i], 0, BUFFER_LEN);
        strncpy(strings[i], buf, BUFFER_LEN - 1);
        ++i;
    }

    int state = strncmp("running", strings[0], 7) == 0 ? 1 : 0;
    int num   = -1;
    sscanf(strings[1], "%d", &num);

    char title_buf[BUFFER_LEN]  = {0};
    char artist_buf[BUFFER_LEN] = {0};

    if (i == 6) // both artist and song-title
    {
        strncpy(title_buf, strings[3], BUFFER_LEN - 1);
        strncpy(artist_buf, strings[2], BUFFER_LEN - 1);
    } else if (i == 5) // artist or song-title is empty
    {
        strncpy(title_buf, strings[2], BUFFER_LEN - 1);
    }

    size_t title_len  = strlen(title_buf);
    size_t artist_len = strlen(artist_buf);

    struct ast_event *e = ast_player_state_long_event_new(
        state, num, title_buf, title_len, artist_buf, artist_len);
    add_to_event_list(e);
#undef BUFFER_LEN
}

static int parse_extended_params(char *params_string,
                                 struct ast_song_parameters_ext *ext)
{
    ext->line_count  = ast_line_count_default;
    ext->vip_song    = 0;
    ext->entry_id[0] = '\0';

    if (strcmp(params_string, "_") == 0) {
        return 0;
    }

    int res        = 0;
    int tokens_len = 0;
    sds *tokens =
        sdssplitlen(params_string, strlen(params_string), ",", 1, &tokens_len);
    for (int i = 0; i < tokens_len; ++i) {
        char key[128], value[128];
        if (sscanf(tokens[i], "%127[^:]:%127[^:]", key, value) == 2) {
            if (strcmp(key, "lines") == 0) {
                switch (atoi(value)) {
                case 2:
                    ext->line_count = ast_line_count_2;
                    break;
                case 4:
                    ext->line_count = ast_line_count_4;
                    break;
                }
            } else if (strcmp(key, "vip_song") == 0) {
                ext->vip_song = atoi(value);
            } else if (strcmp(key, "entry_id") == 0) {
                size_t len = strlen(value);
                if (len > AST_SONG_ENTRY_ID_MAX_LEN) {
                    DBG("the song entry id is too long\n");
                    res = -EINVAL;
                    break;
                } else {
                    memcpy(ext->entry_id, value, len);
                    ext->entry_id[len] = '\0';
                }
            }
        } else {
            DBG("failed to split a token into a key-value pair\n");
            res = -EINVAL;
            break;
        }
    }
    sdsfreesplitres(tokens, tokens_len);
    return res;
}

static void process_reserve_list_message(char *message_text)
{
    int count = 0;
    for (int i = 0; message_text[i]; i++) {
        count += (message_text[i] == '\t');
    }
    struct ast_event *e = ast_reserv_event_new(count);
    if (!e) {
        return;
    }

    sds other_parameters = sdsempty();
    int num, key, tempo, melody;
    int song_num                     = 0;
    char uid[AST_SONG_UID_SIZE + 32] = {};
    int err                          = 0;

    char *token = strtok(message_text, "\t");
    while (token && !err) {
        size_t token_len = strlen(token);
        if (sdslen(other_parameters) < token_len) {
            other_parameters = sdsgrowzero(other_parameters, token_len);
        }
        if (sscanf(token, "%d %d %d %d %s %32s", &num, &key, &tempo, &melody,
                   other_parameters, uid) != 6) {
            if (strstr(token, "reserve-list") != token) {
                err = -EINVAL;
                DBG("failed to split a reserve list entry into parts\n");
            }
        } else {
            struct ast_song_parameters_ext ext;
            err = parse_extended_params(other_parameters, &ext);
            if (err) {
                DBG("failed to parse extended reserve song params\n");
            } else {
                struct ast_song_parameters song = {num, key, tempo, melody,
                                                   ext};
                memcpy(song.uid, uid, min(strlen(uid), AST_SONG_UID_SIZE));
                e->event_data.reserv.songs[song_num++] = song;
            }
        }

        token = strtok(NULL, "\t");
    }

    sdsfree(other_parameters);

    if (!err) {
        add_to_event_list(e);
    }
}

static void process_volume(char *message_text)
{
    int volume = 0;
    if (sscanf(message_text, "volume %d", &volume)) {
        struct ast_event *e = ast_event_new(AST_EVENT_VOLUME);
        if (!e)
            return;

        e->event_data.volume = volume;

        add_to_event_list(e);
    }
}

static void process_mic_effects_message(char *message_text)
{
    DBG("process_mic_efffects_message: %s\n", message_text);
    char effect_name[10] = {};
    int level            = 0;
    int volume           = 0;
    int volume2          = -1;
    struct ast_event *e  = ast_event_new(AST_EVENT_MIC_EFFECT_STATE);
    if (!e) {
        return;
    }
    if (api->features & volume_v2) {
        if (!sscanf(message_text, "%*s %9s %d %d %d", effect_name, &level,
                    &volume, &volume2)) {
            DBG("cannot parse mic_effects_message, terminate!");
            return;
        }
    } else {
        if (!sscanf(message_text, "%*s %9s %d %d", effect_name, &level,
                    &volume)) {
            DBG("cannot parse mic_effects_message, terminate!");
            return;
        }
    }
    strncpy(e->event_data.effect.mic_effect, effect_name, 10);
    e->event_data.effect.mic_level    = level;
    e->event_data.effect.mic_volume   = volume;
    e->event_data.effect.mic_volume_2 = volume2;

    add_to_event_list(e);
}

static void process_error_message(char *message_text)
{
    DBG_PRINTF("error %s: %s", __FUNCTION__, message_text);
    char parsed_error_text[256] = {};
    if (sscanf(message_text, "error %255s", parsed_error_text)) {
        struct RuntimeErrorCode {
            char *error_msg;
            int error_code;
        };

        struct RuntimeErrorCode known_errors[] = {
            {"command-denied-in-non-karaoke-mode",
             AST_RUNTIME_ERROR_NON_KARAOKE_MODE},
            {"pin-not-accepted", AST_RUNTIME_ERROR_PIN_NOT_ACCEPTED},
            {"alternate.txt not found", AST_ERROR_DATABASE_INFO},
            {"certificate-error", AST_ERROR_CERTIFICATE_INFO},
            {"invalid-request", AST_RUNTIME_ERROR_INVALID_REQUEST}};

        int error_code = AST_RUNTIME_ERROR_UNKNOWN;
        for (int i = 0;
             i < sizeof(known_errors) / sizeof(struct RuntimeErrorCode); i++) {
            if (strcmp(known_errors[i].error_msg, parsed_error_text) == 0) {
                error_code = known_errors[i].error_code;
            }
        }

        struct ast_event *e = ast_event_new(AST_EVENT_ERROR);
        if (!e)
            return;

        e->event_data.error = error_code;

        add_to_event_list(e);

        if (error_code == AST_ERROR_CERTIFICATE_INFO) {
            state_error(AST_ERROR_CERTIFICATE_INFO);
        }
    }
}

static void process_counter_message(char *message_text)
{
    int counter = 0;
    if (sscanf(message_text, "counter %d", &counter)) {
        struct ast_event *e = ast_event_new(AST_EVENT_COUNTER);
        if (!e)
            return;

        e->event_data.counter = counter;

        add_to_event_list(e);
    }
}

static int parse_array_of_ints(char *input, int **out, size_t *out_length)
{
    assert(input);
    assert(out);
    assert(out_length);

    int numbers_length = 0;
    sds *numbers = sdssplitlen(input, strlen(input), " ", 1, &numbers_length);
    if (!numbers)
        return -ENOMEM;

    if (numbers_length == 0) {
        *out_length = 0;
        free(numbers);
        return 0;
    }

    int *res = malloc(sizeof(int) * numbers_length);
    if (!res) {
        goto nomem;
    }

    for (int i = 0, index = 0; i < numbers_length; i++) {
        sds n      = numbers[i];
        int number = atoi(n);
        if (number != 0) {
            res[index++] = number;
        }

        sdsfree(n);
    }

    free(numbers);

    *out_length = numbers_length;
    *out        = res;

    return 0;

nomem:
    if (numbers) {
        for (int i = 0; i < numbers_length; i++) {
            sdsfree(numbers[i]);
        }

        free(numbers);
    }

    return -ENOMEM;
}

static void process_toplist_message(char *message_text)
{
    size_t header_length = strlen("top-list");
    char *first_number =
        message_text + header_length + 1; // + 1 to skip first space

    int *numbers = NULL;
    size_t count = 0;
    if (parse_array_of_ints(first_number, &numbers, &count) != 0)
        return;

    if (count > 0) {
        struct ast_event *e = ast_toplist_event_new(count);
        memcpy(e->event_data.top_songs.songs, numbers, sizeof(int) * count);
        free(numbers);

        add_to_event_list(e);
    }
}

static void process_blacklist_message(char *message_text)
{
    size_t header_length = strlen("blacklist-list");
    char *first_number =
        message_text + header_length + 1; // + 1 to skip first space

    int *numbers = NULL;
    size_t count = 0;
    if (parse_array_of_ints(first_number, &numbers, &count) != 0)
        return;

    struct ast_event *e =
        ast_toplist_event_new(count); // layout of both structers is the same
    e->event_type = AST_EVENT_BLACKLIST;
    if (count > 0) {
        memcpy(e->event_data.top_songs.songs, numbers, sizeof(int) * count);
        free(numbers);
    }

    add_to_event_list(e);
}

void process_blacklist_state_message(char *message_text)
{
    int state = 0;
    if (sscanf(message_text, "blacklist-state %d", &state)) {
        struct ast_event *e = ast_event_new(AST_EVENT_BLACKLIST_STATE);
        if (!e)
            return;

        e->event_data.blacklist_enabled = state;

        add_to_event_list(e);
    }
}

static void process_bgastate_message(char *message_text)
{
    size_t firstQuoteIndex  = (size_t)-1;
    size_t secondQuoteIndex = (size_t)-1;
    for (size_t i = 0, count = strlen(message_text); i < count; i++) {
        if (message_text[i] == '\"') {
            if (firstQuoteIndex == (size_t)-1) {
                firstQuoteIndex = i;
            } else {
                secondQuoteIndex = i;
                break;
            }
        }
    }

    if ((firstQuoteIndex == (size_t)-1) || (secondQuoteIndex == (size_t)-1)) {
        DBG("Invalid bga-state message received (there are no quotes around "
            "second "
            "parameter): %s",
            message_text);
        return;
    }

    assert(secondQuoteIndex > firstQuoteIndex);

    sds url = sdsnew(message_text);
    sdsrange(url, firstQuoteIndex + 1, secondQuoteIndex - 1);

    // Since %s scanf reads upto first whitespace,
    // we can not use because input buffer can contain
    // spaces in 'url' parameter, so we have to cut it from
    // the input buffer

    sds first_part = sdsnew(message_text);
    sdsrange(first_part, 0, firstQuoteIndex - 1);

    sds second_part = sdsnew(message_text);
    sdsrange(second_part, secondQuoteIndex + 2, -1);

    sds msg = sdscatsds(first_part, second_part);

    char statestr[12] = {0};
    int position      = 0;
    int duration      = 0;

    if (sscanf(msg, "bga-state %11s %d %d", statestr, &position, &duration)) {
        enum ast_bga_state state = ast_bgastate_disabled;
        if (strcmpi(statestr, "idle") == 0)
            state = ast_bgastate_idle;
        else if (strcmpi(statestr, "playing") == 0)
            state = ast_bgastate_playing;
        else if (strcmpi(statestr, "paused") == 0)
            state = ast_bgastate_paused;

        struct ast_event *e =
            ast_bgastate_event_new(state, url, sdslen(url), position, duration);
        if (e) {
            add_to_event_list(e);
        }
    }

    sdsfree(msg);
    sdsfree(second_part);
    sdsfree(url);
}

static void process_bgvstate_message(char *message_text)
{

    char statestr[12] = {0};
    char modestr[13]  = {0};
    int position      = 0;

    if (sscanf(message_text, "bgv-state %11s %12s %d", statestr, modestr,
               &position)) {
        enum ast_bgv_state state = ast_bgvstate_disabled;
        if (strcmpi(statestr, "idle") == 0)
            state = ast_bgvstate_idle;
        else if (strcmpi(statestr, "playing") == 0)
            state = ast_bgvstate_playing;

        enum ast_bgv_mode mode = ast_bgvmode_background;
        if (strcmpi(modestr, "clip") == 0)
            mode = ast_bgvmode_clip;
        else if (strcmpi(modestr, "capture") == 0)
            mode = ast_bgvmode_capture;
        struct ast_event *e = ast_bgvstate_event_new(state, mode, position);
        if (e) {
            add_to_event_list(e);
        }
    }
}

static void process_reclist_message(char *message_text)
{
    size_t msg_len = strlen(message_text), rec_count = 0, index = 0;
    for (char *p = message_text; *p; ++p)
        rec_count += (*p == '\v');
    char *str_pool_ptr = NULL;
    struct ast_event *e =
        ast_recordingslist_event_new(rec_count, msg_len, &str_pool_ptr);
    if (!e || !str_pool_ptr)
        return;
    char *item = strtok(message_text, "\v");
    while ((item = strtok(NULL, "\v"))) {
        struct ast_recording_info info = {0, 0, 0, 0, 0, 0, NULL, NULL};
        int *pnums[]                   = {&info.id,  &info.song,  &info.date,
                        &info.key, &info.tempo, &info.melody};
        const char **ptexts[]          = {&info.title, &info.singer};
        const size_t num_count         = sizeof(pnums) / sizeof(pnums[0]),
                     text_count        = sizeof(ptexts) / sizeof(ptexts[0]);
        char *current                  = item, *next;
        for (size_t i = 0; current && i < num_count + text_count;
             ++i, current = next) {
            if ((next = strchr(current, '\t')))
                *next++ = '\0';
            const char *token = *current == '\t' ? "" : current;
            if (i < num_count) {
                *pnums[i] = atoi(token);
            } else {
                size_t len = strlen(token) + 1;
                strncpy(str_pool_ptr, token, len);
                *ptexts[i - num_count] = str_pool_ptr;
                str_pool_ptr += len;
            }
        }
        if (info.id <= 0 || info.song <= 0 || !info.title || !info.singer) {
            DBG("invalid recordings-list message");
            ast_event_delete(e);
            return;
        }
        e->event_data.recordings.rec[index++] = info;
    }
    add_to_event_list(e);
}

static void process_message(char *message_text)
{
    struct MessageProcessor {
        char *message_header;
        void (*process)(char *message);
    };

    struct MessageProcessor message_processors[] = {
        {"fwinfo", on_fw_info_response},
        {"certinfo", on_certinfo_response},
        {"featuresinfo", on_features_response},
        {"effect-list", on_effect_list_response},
        {"dbinfo", on_dbinfo_response},
        {"alterinfo", on_alterinfo_response},

        {"player-state-long", process_player_state_long_message},
        {"player-state", process_player_state_message},
        {"reserve-list", process_reserve_list_message},
        {"mic-effect", process_mic_effects_message},
        {"volume", process_volume},
        {"error", process_error_message},
        {"counter", process_counter_message},
        {"top-list", process_toplist_message},
        {"blacklist-list", process_blacklist_message},
        {"blacklist-state", process_blacklist_state_message},
        {"bga-state", process_bgastate_message},
        {"bgv-state", process_bgvstate_message},
        {"recordings-list", process_reclist_message}};

    for (int i = 0;
         i < sizeof(message_processors) / sizeof(struct MessageProcessor);
         i++) {
        struct MessageProcessor proc = message_processors[i];

        if (strncmp(proc.message_header, message_text,
                    strlen(proc.message_header)) == 0) {
            proc.process(message_text);
            break;
        }
    }
}

static void on_message_read(uv_stream_t *stream, ssize_t nread,
                            const uv_buf_t *buf)
{
    if (nread < 0) {
#ifdef _DEBUG
        // int err = uv_last_error(api->uvloop);
        DBG("on_message_read error occured: %d\n", (int)nread);
#endif //_DEBUG
        state_error(AST_ERROR_CONNECT);
        free(buf->base);
        return;
    }

    g_last_message_time = time(0);

    const char *sep      = "\r\n";
    const size_t sep_len = strlen(sep);
    char *p              = buf->base;
    while (p < (buf->base + (size_t)nread)) {
        char *t = strstr(p, sep);
        if (t) {
            size_t message_len = (size_t)(t - p);

            sds msg = NULL;

            if (api->large_buffer) {
                msg = sdscatlen(api->large_buffer, p, message_len);
                api->large_buffer = NULL;
            } else
                msg = sdsnewlen(p, message_len);
            if (!msg) {
                DBG("sdsnewlen return NULL, possible no memory");
                return;
            }
            process_message(msg);
            sdsfree(msg);

            p = t + sep_len;
        } else {
            // Separator not found, may be we have got a part of mega-message
            size_t len = (size_t)((buf->base + (size_t)nread) - p);
            if (len > 0) {
                if (!api->large_buffer)
                    api->large_buffer = sdsnewlen(p, len);
                else
                    api->large_buffer = sdscatlen(api->large_buffer, p, len);

                p += len;
            }
        }
    }

    free(buf->base);
}

static void state_online()
{
    DBG("online");
    api->state = ast_api_online;
    add_to_event_list(ast_event_new(AST_EVENT_CONNECTED));
    subscribe();
    uv_read_start((uv_stream_t *)api->tcp, astapi_custom_alloc,
                  on_message_read);
}

static void handle_close_cb(uv_handle_t *handle)
{
    DBG("calling free() on %p", handle);
    free(handle);
}

static void close_sockets()
{
    DBG("closing sockets");
    if (api->tcp) {
        uv_close((uv_handle_t *)api->tcp, handle_close_cb);
        api->tcp = NULL;
    }
    if (api->udp) {
        uv_close((uv_handle_t *)api->udp, handle_close_cb);
        api->udp = NULL;
    }
}

static void state_offline()
{
    DBG("offline");
    if (api->state != ast_api_offline && api->state != ast_api_error) {
        if (api->state != ast_api_discovering) {
            unsubscribe();
        }
        close_sockets();
    }
    api->state = ast_api_offline;
    add_to_event_list(ast_event_new(AST_EVENT_DISCONNECTED));
}

static void state_error(int err)
{
    DBG("error %d", err);
    unsubscribe();
    close_sockets();
    api->state          = ast_api_error;
    struct ast_event *e = ast_event_new(AST_EVENT_ERROR);
    if (!e)
        return;
    e->event_data.error = err;
    add_to_event_list(e);
    if (err == AST_ERROR_CONNECT) {
        start_reconnect_timer(5000);
    }
}

void add_device_to_list(const struct ast_device_info *dev)
{
    const int len = api->device_list.count;
    for (int i = 0; i < len; ++i) {
        if (memcmp(dev, &api->device_list.dev[i], sizeof(*dev)) == 0) {
            return;
        }
    }
    if (len < AST_DEVICE_LIST_LIMIT) {
        memcpy(&api->device_list.dev[len], dev, sizeof(*dev));
        api->device_list.count += 1;
    }
}

static void on_info_read(uv_udp_t *handle, ssize_t nread, const uv_buf_t *buf,
                         const struct sockaddr *addr, unsigned flags)
{
    DBG("on_info_read %p nread=%d addr=%p flags=%u", handle, (int)nread, addr,
        flags);
    uint32_t *cmd = (uint32_t *)buf->base;

    if (nread == 60 && cmd[0] == 0xAAACAAAB && addr &&
        (api->state == ast_api_discovering || api->state == ast_api_error ||
         api->state == ast_api_offline)) {
        char sender[16];
        int iperr =
            uv_ip4_name((struct sockaddr_in *)addr, sender, sizeof(sender));
        if (iperr) {
            DBG("uv_ip4_name error %d", iperr);
            return;
        }

        char serial[13] = {0};
        for (int i = 1; i <= 12; ++i)
            serial[i - 1] = (char)cmd[i];

        DBG("model=%u version=%u serial=%s ip=%s", cmd[13], cmd[14], serial,
            sender);
        if (cmd[14] > 0x30000 && cmd[14] < 0x40000) {
            if (is_scanning()) {
                struct ast_device_info dev;
                dev.model = cmd[13];
                strncpy(dev.ip, sender, sizeof(dev.ip));
                strncpy(dev.sn, serial, sizeof(dev.sn));
                DBG("add device %s to list", sender);
                add_device_to_list(&dev);
            } else {
                uv_udp_recv_stop(api->udp);
                api->state = ast_api_connecting;
                stop_reconnect_timer();
                api->tcp = malloc(sizeof(*api->tcp));
                uv_tcp_init(api->uvloop, api->tcp);
                struct sockaddr_in send_addr;
                uv_ip4_addr(api->hostname, 2202, &send_addr);
                uv_tcp_connect(&api->tcp_connect, api->tcp,
                               (const struct sockaddr *)&send_addr, on_connect);
            }
        } else {
            stop_reconnect_timer();
            close_sockets();
            api->state          = ast_api_error;
            struct ast_event *e = ast_event_new(AST_EVENT_ERROR);
            if (!e)
                return;
            e->event_data.error = AST_ERROR_INCOMPATIBLE_FIRMWARE_VERSION;
            add_to_event_list(e);
        }
    }
    if (buf->len != 0) {
        free(buf->base);
    }
}

int ast_connect(const char *hostname)
{
    if (strlen(hostname) == 0 ||
        (api->state != ast_api_offline && api->state != ast_api_error))
        return -1;
    DBG("connect %s", hostname);
    api->state = ast_api_discovering;
    if (api->hostname)
        free(api->hostname);
    api->hostname = strdup(hostname);
    start_reconnect_timer(0);
    return 0;
}

int ast_disconnect()
{
    DBG("disconnect");

    stop_reconnect_timer();
    state_offline();

    free(api->hostname);
    api->hostname = NULL;

    return 0;
}

int ast_scan()
{
    if (api->state != ast_api_offline)
        return -1;
    api->state = ast_api_discovering;
    start_scan_timer(SCAN_TIME);
    return udp_connect();
}

int ast_cancel_scan()
{
    if (api->state != ast_api_discovering) {
        return -1;
    }
    if (uv_timer_stop(&api->scan_timer) != 0) {
        return -1;
    }
    api->device_list.count = 0;
    api->state             = ast_api_offline;
    return 0;
}

int udp_init()
{
    if (api->udp) {
        return 0;
    }

    api->udp = malloc(sizeof(*api->udp));
    if (!api->udp) {
        return -ENOMEM;
    }

    uv_udp_init(api->uvloop, api->udp);
    struct sockaddr_in send_addr;
    uv_ip4_addr("0.0.0.0", 0, &send_addr);
    int err = uv_udp_bind(api->udp, (const struct sockaddr *)&send_addr, 0);
    if (err < 0) {
        DBG("udp bind error");
        state_error(AST_ERROR_NETWORK_INIT);
    }
    return err;
}

static int udp_connect()
{
    int err = udp_init();
    if (err) {
        DBG("udp_init error code=%d", err);
        return err;
    }

    err = send_info_request();
    if (err) {
        DBG("send_info request error code=%d", err);
        return err;
    }

    err = uv_udp_recv_start(api->udp, astapi_custom_alloc, on_info_read);
    if (err) {
        DBG("uv_udp_recv_start error code=%d", err);
        return err;
    }

    return 0;
}

static void on_udp_bcast_sent(uv_udp_send_t *req, int status)
{
    if (status < 0) {
        DBG("uv_udp_sent error code=%d error:%s", status, uv_strerror(status));
        api->discovery_state.last_time_broadcasts_failed += 1;
    }
}

enum ast_api_state ast_state()
{
    if (api == NULL) {
        return ast_api_offline;
    }
    return api->state;
}

int ast_event_pull(struct ast_event **ppevent)
{
    time_t current_time           = time(0);
    double time_from_last_message = difftime(current_time, g_last_message_time);
    if (api->state == ast_api_online &&
        time_from_last_message > 2 * KEEP_ALIVE_TIME / 1000) {
        state_error(AST_ERROR_CONNECT);
    }

    uv_run(api->uvloop, UV_RUN_NOWAIT);
    if (api->event_list) {
        struct ast_event *e = api->event_list;
        *ppevent            = e;
        api->event_list     = e->event_next;
        e->event_next       = 0;
        return 1;
    }
    return 0;
}

int ast_event_free(struct ast_event **ppevent)
{
    struct ast_event *e = *ppevent;
    if (e) {
        ast_event_delete(e);
        *ppevent = 0;
    }
    return 0;
}

static void on_send_msg(uv_write_t *req, int status)
{
    if (status) {
#ifdef _DEBUG
        // int err = uv_last_error(api->uvloop);
        DBG("uv_write error: %d\n", status);
#endif
    }
#ifdef _WIN32
    free(req->write_buffer.base);
#endif
    free(req);
}

static int send_msg(char *s)
{
    if (api->tcp == NULL || !uv_is_writable((uv_stream_t *)api->tcp)) {
#ifdef _DEBUG
        DBG("api->tcp is null or not writable, but send_msg %s\n", s);
#endif
#ifdef __ANDROID__
        const char *wrn_text = "Call send_msg while TCP handle is not ready";
        __android_log_print(ANDROID_LOG_WARN, "native", "%s\n", wrn_text);
#endif
        return UV_EAGAIN;
    }
    return send_request(s, (uv_stream_t *)api->tcp, on_send_msg);
}

static void on_keepalive_timeout(uv_timer_t *handle)
{
    // DBG("keep alive timeout");
    send_msg("player-state\r\n");
}

static void start_keepalive_timer()
{
    start_timer(&api->keep_alive_timer, on_keepalive_timeout, KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME);
}

static int all_udp_broadcasts_failed()
{
    return (api->discovery_state.last_time_broadcasts_failed > 0 &&
            (api->discovery_state.last_time_broadcasts_sent ==
             api->discovery_state.last_time_broadcasts_failed));
}

static void on_scan_timeout(uv_timer_t *handle)
{
    DBG("stop_devices_search");
    uv_udp_recv_stop(api->udp);
    if (all_udp_broadcasts_failed()) {
        api->discovery_state.last_time_broadcasts_sent   = 0;
        api->discovery_state.last_time_broadcasts_failed = 0;
        struct ast_event *e = ast_event_new(AST_EVENT_ERROR);
        if (!e) {
            return;
        }
        e->event_data.error = AST_RUNTIME_ERROR_BROADCAST_FAILED;
        add_to_event_list(e);
    } else {
        struct ast_event *e = calloc(1, sizeof(struct ast_event) +
                                            sizeof(struct ast_devices_list));
        e->event_type       = AST_EVENT_DEVICES_LIST;
        e->event_next       = NULL;
        memcpy(&e->event_data.devices, &api->device_list,
               sizeof(struct ast_devices_list));
        add_to_event_list(e);
    }
    api->state             = ast_api_offline;
    api->device_list.count = 0;
}

static void start_scan_timer(uint64_t timeout)
{
    DBG("start devices search");
    start_timer(&api->scan_timer, on_scan_timeout, timeout, 0);
}

void subscribe()
{
    send_msg("subscribe\r\n");
    start_keepalive_timer();
    g_last_message_time = time(0);
}

void unsubscribe()
{
    uv_timer_stop(&api->keep_alive_timer);
    send_msg("unsubscribe\r\n");
}

int ast_ask_player_state_long()
{
    if (!api)
        return -EINVAL;

    char buf[128];
    snprintf(buf, sizeof(buf) - 1, "player-state-long\r\n");

    return send_msg(buf);
}

static sds format_song_parameters(const struct ast_song_parameters *params)
{
    assert(params);

    sds other_params =
        sdscatprintf(sdsempty(), "vip_song:%d", params->ext.vip_song);
    switch (params->ext.line_count) {
    case ast_line_count_2:
        other_params = sdscat(other_params, ",lines:2");
        break;
    case ast_line_count_4:
        other_params = sdscat(other_params, ",lines:4");
        break;
    }

    if (params->ext.entry_id[0] != '\0') {
        assert(strlen(params->ext.entry_id) <= AST_SONG_ENTRY_ID_MAX_LEN);
        other_params =
            sdscatprintf(other_params, ",entry_id:%s", params->ext.entry_id);
    }

    // Always supply a placeholder. This is mandatory for parsing
    // "reserve-set-list" arguments and is fine for all other cases.
    char uidbuf[AST_SONG_UID_SIZE + 1];
    if (params->uid[0] != '\0') {
        strncpy(uidbuf, params->uid, AST_SONG_UID_SIZE);
        uidbuf[AST_SONG_UID_SIZE] = '\0';
    } else {
        uidbuf[0] = '_';
        uidbuf[1] = '\0';
    }

    sds ret =
        sdscatprintf(sdsempty(), "%u %d %d %d %s %s", params->num, params->key,
                     params->tempo, params->melody, other_params, uidbuf);
    sdsfree(other_params);
    return ret;
}

int ast_play(const struct ast_song_parameters *params)
{
    sds buf = sdsempty();
    if (!api)
        return -EINVAL;
    if (!params)
        buf = sdscpy(buf, "player-play\r\n");
    else {
        sds song_params_formatted = format_song_parameters(params);
        buf = sdscatprintf(buf, "player-play %s\r\n", song_params_formatted);
        sdsfree(song_params_formatted);
    }
    const int sendres = send_msg(buf);
    sdsfree(buf);
    return sendres;
}

int ast_play_next()
{
    int err = ast_stop();
    if (!err)
        err = ast_play(0);
    return err;
}

int ast_pause()
{
    if (!api)
        return -EINVAL;
    return send_msg("player-pause\r\n");
}

int ast_stop()
{
    if (!api)
        return -EINVAL;
    return send_msg("player-stop\r\n");
}

static int reserve(const struct ast_song_parameters *params, int prior)
{
    if (!api)
        return -EINVAL;

    DBG("AstApi: reserve()");

    sds buf                   = sdsempty();
    char *command             = prior ? "reserve-priority" : "reserve-add";
    sds song_params_formatted = format_song_parameters(params);
    buf = sdscatprintf(buf, "%s %s\r\n", command, song_params_formatted);
    sdsfree(song_params_formatted);
    DBG("reserve: %s", buf);
    const int sendres = send_msg(buf);
    sdsfree(buf);
    DBG("Return from reserve()");
    return sendres;
}

int ast_reserve(const struct ast_song_parameters *params)
{
    assert(params);
    return reserve(params, 0);
}

int ast_priority_reserve(const struct ast_song_parameters *params)
{
    assert(params);
    return reserve(params, 1);
}

int ast_reserve_modify(const struct ast_song_parameters *params, int position,
                       enum reserve_modify_option option)
{
    assert(params);
    if (!api)
        return -EINVAL;

    const char *option_key = "key";
    sds value              = 0;

    switch (option) {
    case reserve_modify_option_key:
        option_key = "key";
        value      = sdscatprintf(sdsempty(), "%d", params->key);
        break;
    case reserve_modify_option_tempo:
        option_key = "tempo";
        value      = sdscatprintf(sdsempty(), "%d", params->tempo);
        break;
    case reserve_modify_option_melody:
        option_key = "melody";
        value      = sdscatprintf(sdsempty(), "%d", params->melody);
        break;
    case reserve_modify_option_line_count:
        option_key = "lines";
        switch (params->ext.line_count) {
        case ast_line_count_2:
            value = sdscpy(sdsempty(), "2");
            break;
        case ast_line_count_4:
            value = sdscpy(sdsempty(), "4");
            break;
        default:
            value = sdscpy(sdsempty(), "_");
        }
        break;
    case reserve_modify_option_number:
        option_key = "number";
        value      = sdscatprintf(sdsempty(), "%d", params->num);
        break;
    }

    sds buf = sdscatprintf(sdsempty(), "%s %u %s %s\r\n", "reserve-modify",
                           position, option_key, value);

    sdsfree(value);

    int ret = send_msg(buf);
    DBG("reserve: %s", buf);
    sdsfree(buf);
    return ret;
}

int ast_reserve_move(int pos, int to)
{
    if (pos == to)
        return 0;

    char buf[256];
    sprintf(buf, "reserve-move %d %d\r\n", pos, to);
    DBG("%s", buf);
    return send_msg(buf);
}

int ast_reserve_delete(int pos)
{
    char buf[256];
    sprintf(buf, "reserve-delete %d\r\n", pos);
    DBG("%s", buf);
    return send_msg(buf);
}

int ast_reserve_clear()
{
    if (!api)
        return -EINVAL;

    char *command = "reserve-clear\r\n";

    return send_msg(command);
}

int ast_reserve_pause(int value)
{
    if (!api)
        return -EINVAL;

    sds buf = sdscatprintf(sdsempty(), "%s %d\r\n", "reserve-pause", value);

    int res = send_msg(buf);

    sdsfree(buf);

    return res;
}

int ast_reserve_set(const struct ast_song_parameters *songs, int num_songs)
{
    assert(num_songs >= 0);

    sds buf = sdsempty();
    buf     = sdscat(buf, "reserve-set-list");
    for (int i = 0; i < num_songs; ++i) {
        buf           = sdscat(buf, " ");
        sds formatted = format_song_parameters(songs + i);
        buf           = sdscatsds(buf, formatted);
        sdsfree(formatted);
    }

    buf     = sdscat(buf, "\r\n");
    int err = send_msg(buf);
    sdsfree(buf);
    return err;
}

int ast_bgvideo_switch_mode()
{
    if (!api || api->state != ast_api_online)
        return -EINVAL;

    return send_msg("bgvideo-switch-mode\r\n");
}

int ast_bgvideo_play(const char *url)
{
    if ((!api) || (!url))
        return -EINVAL;

    sds buf = sdscatprintf(sdsempty(), "%s %s\r\n", "bgv-play-url", url);

    int res = send_msg(buf);

    sdsfree(buf);

    return res;
}

int ast_bgaudio_play(const char *url)
{
    if ((!api) || (!url))
        return -EINVAL;

    sds buf = sdscatprintf(sdsempty(), "%s %s\r\n", "bga-play-url", url);

    int res = send_msg(buf);

    sdsfree(buf);

    return res;
}

int ast_bgaudio_applause()
{
    if ((!api))
        return -EINVAL;
    return send_msg("press APPLAUSE\r\n");
}

int ast_bgaudio_stop()
{
    if (!api || api->state != ast_api_online)
        return -EINVAL;

    return send_msg("bga-stop\r\n");
}

int ast_bgaudio_pause()
{
    if (!api)
        return -EINVAL;

    return send_msg("bga-pause\r\n");
}

int ast_bgaudio_resume()
{
    if (!api)
        return -EINVAL;

    return send_msg("bga-resume\r\n");
}

int ast_bgaudio_set_volume(int value)
{
    if (!api)
        return -EINVAL;

    sds buf = sdscatprintf(sdsempty(), "%s %d\r\n", "bga-volume", value);

    int res = send_msg(buf);

    sdsfree(buf);

    return res;
}

int ast_recording_start_replay(int id)
{
    if (!api)
        return -EINVAL;

    char buf[256];
    sprintf(buf, "recording-start-replay %d\r\n", id);
    DBG("%s", buf);
    return send_msg(buf);
}

int ast_recording_stop_replay()
{
    if (!api)
        return -EINVAL;

    DBG("recording-stop-replay");
    return send_msg("recording-stop-replay\r\n");
}

int ast_recording_remove(int id)
{
    if (!api)
        return -EINVAL;

    char buf[256];
    sprintf(buf, "recording-remove %d\r\n", id);
    DBG("%s", buf);
    return send_msg(buf);
}

int ast_recordings_clear()
{
    if (!api)
        return -EINVAL;

    DBG("recordings-clear");
    return send_msg("recordings-clear\r\n");
}

int ast_songcounter()
{
    if (!api)
        return -EINVAL;

    return send_msg("counter\r\n");
}

int ast_toplist()
{
    if (!api)
        return -EINVAL;

    return send_msg("top-list\r\n");
}

int ast_blacklist_state()
{
    if (!api)
        return -EINVAL;

    DBG("%s", "blacklist-state\r\n");

    return send_msg("blacklist-state\r\n");
}

int ast_blacklist_fetch()
{
    if (!api)
        return -EINVAL;

    DBG("%s", "blacklist-list\r\n");

    return send_msg("blacklist-list\r\n");
}

sds int_array_to_sds(const int *numbers, int num_songs)
{
    sds buf = sdsnew(NULL);
    if (!buf)
        return NULL;

    for (int i = 0; i < num_songs; i++) {
        const size_t max_digit_len = 33;
        sds tmp_buf                = sdsnewlen(NULL, max_digit_len);
        sprintf(tmp_buf, "%d", numbers[i]);
        buf = sdscatlen(buf, tmp_buf, strlen(tmp_buf));
        sdsfree(tmp_buf);
        buf = sdscat(buf, " ");
    }

    return buf;
}

sds format_int_array_command(const char *command, const int *numbers,
                             int num_songs)
{
    assert(command);
    assert(numbers);
    sds msg = sdsnew(command);
    sds buf = int_array_to_sds(numbers, num_songs);
    if (!buf)
        goto end;

    if (!msg)
        goto end;
    msg = sdscat(msg, " ");
    msg = sdscatsds(msg, buf);
    msg = sdscat(msg, "\r\n");

end:
    if (buf) {
        sdsfree(buf);
    }

    return msg;
}

#define name(command) #command

char *sound_command_msg(enum ast_sound_command command)
{
    static char *strings[] = {
        "mic-effect-up\r\n",  "mic-effect-down\r\n", "mic-level-up\r\n",
        "mic-level-down\r\n", "mic-volume-up\r\n",   "mic-volume-down\r\n",
        "tone-up\r\n",        "tone-down\r\n",       "tempo-up\r\n",
        "tempo-down\r\n",     "melody-up\r\n",       "melody-down\r\n",
        "volume-up\r\n",      "volume-down\r\n"};

    return strings[command];
}

int ast_apply_sound_command(enum ast_sound_command command)
{
    return send_msg(sound_command_msg(command));
}

static int check_value_for_parameter(enum ast_sound_parameter param, int value)
{
    struct limits {
        int min;
        int max;
    };

    static struct limits sound_param_bounds[] = {
        {-3, 3},  //    ast_sound_parameter_mic_volume
        {0, 9},   //    ast_sound_parameter_mic_effect
        {0, 100}, //    ast_sound_parameter_mic_effect_level
        {MINIMUM_VOLUME_VALUE,
         MAXIMUM_VOLUME_VALUE}, //    ast_sound_parameter_volume
        {MINIMUM_TEMPO_VALUE,
         MAXIMUM_TEMPO_VALUE},                  //    ast_sound_parameter_tempo
        {MINIMUM_KEY_VALUE, MAXIMUM_KEY_VALUE}, //    ast_sound_parameter_key
        {MINIMUM_MELODY_VALUE,
         MAXIMUM_MELODY_VALUE}, //    ast_sound_parameter_melody
        {0, 100},               //    ast_sound_parameter_mic_volume_2
        {-7, 7},                //    equalizer gains
        {-7, 7},
        {-7, 7},
    };

    return value >= sound_param_bounds[param].min &&
           value <= sound_param_bounds[param].max;
}

char *format_sound_command(enum ast_sound_parameter param)
{
    static char *strings[] = {"mic-volume",           "mic-effect",
                              "mic-effect-level",     "volume",
                              "player-modify tempo",  "player-modify key",
                              "player-modify melody", "mic-volume-2",
                              "eq-lowband-gain",      "eq-midband-gain",
                              "eq-highband-gain"};

    return strings[param];
}

int ast_mic_effect_count()
{
    assert(api);
    return api->effect_count;
}

const char *ast_mic_effect_string_for_abbriviation(char *abbr)
{
    assert(api && api->effect_count > 0);
    for (int i = 0; i < api->effect_count; ++i) {
        if (strcmp(abbr, api->effect_list[i].abbriviation) == 0) {
            return api->effect_list[i].name;
        }
    }
    DBG("Cannot convert mic_effect_abbriviation %s to mic_effect_name", abbr);
    return api->effect_list[api->effect_count - 1].name;
}

const char *ast_mic_effect_string(int value)
{
    assert(api && (api->effect_count > 0) && (value < api->effect_count));
    return api->effect_list[value].name;
}

const char *ast_mic_effect_abbriviation(int value)
{
    assert(api && (api->effect_count > 0) && (value < api->effect_count));
    return api->effect_list[value].abbriviation;
}

int ast_apply_sound_parameter_value(enum ast_sound_parameter param, int value)
{
    if (check_value_for_parameter(param, value)) {
        char buffer[512];
        char *command = format_sound_command(param);

        (param == ast_sound_parameter_mic_effect)
            ? sprintf(buffer, "%s %s\r\n", command,
                      ast_mic_effect_abbriviation(value))
            : sprintf(buffer, "%s %d\r\n", command, value);

        return send_msg(buffer);
    }
    return -1;
}

int ast_set_pin(int value)
{
    if (!api)
        return -EINVAL;

    sds buf = sdscatprintf(sdsempty(), "%s %d\r\n", "pin-code", value);

    int res = send_msg(buf);

    sdsfree(buf);

    return res;
}

static void on_altersync_response(uv_stream_t *stream, ssize_t nread,
                                  const uv_buf_t *buf)
{
    if (nread < 0) {
        state_error(AST_ERROR_DATABASE_DOWNLOAD);
        fclose(g_dbFile);
    } else {
        api->dbsync_progress += nread;
        fwrite(buf->base, sizeof(char), nread, g_dbFile);
        DBG("%d of %d", (int)api->dbsync_progress, (int)api->dbsync_total);
        if (api->dbsync_progress == api->dbsync_total) {
            uv_read_stop(stream);
            fclose(g_dbFile);
            uv_read_start(stream, astapi_custom_alloc, on_message_read);

            uint32_t actual_crc = crc32_of_file(api->alter_path);
            if (actual_crc == g_defferCRC) {
                // Everything is ok, begin listen device for
                // status updates
                state_online();
            } else {
                // Either crc calculation failure or invalid content downloaded
                sync_alter();
            }
        } else if (api->dbsync_progress > api->dbsync_total) {
            state_error(AST_ERROR_DATABASE_DOWNLOAD);
            fclose(g_dbFile);
        }
    }
    free(buf->base);
}

static void on_altersync_request(uv_write_t *req, int status)
{
    DBG("on_altersync_request %p status=%d", req, status);
    if (status < 0) {
        state_error(AST_ERROR_DATABASE_SYNC);
    } else {
#ifdef _WIN32
        g_dbFile = _wfopen(api->alter_path, L"wb");
#else
        g_dbFile = fopen(api->alter_path, "w");
#endif
        if (g_dbFile == 0) {
            state_error(AST_ERROR_DATABASE_DOWNLOAD);
        } else {
            uv_read_stop((uv_stream_t *)api->tcp);
            uv_read_start((uv_stream_t *)api->tcp, astapi_custom_alloc,
                          on_altersync_response);
        }
    }
}

static void download_alternatives()
{
    DBG("download_alternatives");
    api->state = ast_api_downloading;
    send_request("altersync\r\n", (uv_stream_t *)api->tcp,
                 on_altersync_request);
}

static void on_alterinfo_response(char *message_text)
{
    DBG("on_alterinfo_response %s", message_text);

    uint32_t size, crc;
    if (2 != sscanf(message_text, "alterinfo %x %u", &crc, &size)) {
        state_error(AST_ERROR_DATABASE_INFO);
    } else if (memcmp(api->alter_path, ":skip:", 6) == 0) {
        state_online();
    } else {
        uint32_t local_crc = crc32_of_file(api->alter_path);
        if (crc != local_crc) {
            api->dbsync_progress = 0;
            api->dbsync_total    = size;
            g_defferCRC          = crc;
            download_alternatives();
        } else {
            state_online();
        }
    }
}

static void sync_alter()
{
    DBG("sync_alter");
    send_request("alterinfo\r\n", (uv_stream_t *)api->tcp, 0);
}

static void on_dbsync_response(uv_stream_t *stream, ssize_t nread,
                               const uv_buf_t *buf)
{
    if (nread < 0) {
        state_error(AST_ERROR_DATABASE_DOWNLOAD);
        fclose(g_dbFile);
    } else {
        api->dbsync_progress += nread;
        fwrite(buf->base, sizeof(char), nread, g_dbFile);
        DBG("%d of %d", (int)api->dbsync_progress, (int)api->dbsync_total);
        struct ast_event *event = ast_event_new(AST_EVENT_DOWNLOAD_PROGRESS);
        if (!event)
            return;
        event->event_data.download_progress.current = api->dbsync_progress;
        event->event_data.download_progress.total   = api->dbsync_total;
        add_to_event_list(event);
        if (api->dbsync_progress == api->dbsync_total) {
            uv_read_stop(stream);
            fclose(g_dbFile);
            g_dbFile = 0;
            uv_read_start(stream, astapi_custom_alloc, on_message_read);
            uint32_t actual_crc = crc32_of_file(api->db_path);
            if (actual_crc == g_defferCRC) {
                // Everything is ok, check for alternatives
                sync_alter();
            } else {
                // Either crc calculation failure or invalid content was
                // downloaded
                sync_database();
            }
        } else if (api->dbsync_progress > api->dbsync_total) {
            state_error(AST_ERROR_DATABASE_DOWNLOAD);
            fclose(g_dbFile);
        }
    }
    free(buf->base);
}

static void on_dbsync_request(uv_write_t *req, int status)
{
    DBG("on_dbsync_request %p status=%d", req, status);
    if (status < 0) {
        state_error(AST_ERROR_DATABASE_SYNC);
    } else {
#ifdef _WIN32
        g_dbFile = _wfopen(api->db_path, L"wb");
#else
        g_dbFile = fopen(api->db_path, "w");
#endif
        if (g_dbFile == 0) {
            state_error(AST_ERROR_DATABASE_DOWNLOAD);
        } else {
            uv_read_stop((uv_stream_t *)api->tcp);
            uv_read_start((uv_stream_t *)api->tcp, astapi_custom_alloc,
                          on_dbsync_response);
        }
    }
}

static void download_database()
{
    DBG("download database");
    api->state = ast_api_downloading;
    add_to_event_list(ast_event_new(AST_EVENT_DATABASE_DOWNLOAD));
    send_request("dbsync\r\n", (uv_stream_t *)api->tcp, on_dbsync_request);
}

static void on_dbinfo_response(char *message_text)
{
    DBG("on_dbinfo_response %p: %s", message_text, message_text);

    uint32_t size, crc;
    if (2 != sscanf(message_text, "dbinfo %x %u", &crc, &size)) {
        state_error(AST_ERROR_DATABASE_INFO);
    } else if (memcmp(api->alter_path, ":skip:", 6) == 0) {
        sync_alter();
    } else {
        uint32_t local_crc = crc32_of_file(api->db_path);
        if (crc != local_crc) {
            api->dbsync_progress = 0;
            api->dbsync_total    = size;
            g_defferCRC          = crc;
            download_database();
        } else {
            sync_alter();
        }
    }
}

static void sync_database()
{
    DBG("sync_database");
    send_request("dbinfo\r\n", (uv_stream_t *)api->tcp, 0);
}

struct ast_event *ast_cert_dates_event_new(int intervals_count)
{
    size_t memory_size =
        sizeof(struct ast_event) +
        sizeof(struct ast_event_certified_dates) * intervals_count;
    struct ast_event *e = calloc(1, memory_size);
    if (!e) {
        return NULL;
    }
    e->event_type                     = AST_EVENT_CERTIFIED_MONTHES;
    e->event_data.dates.num_intervals = intervals_count;

    return e;
}

static void process_cert_dates(char *dates)
{
    int intervals_count = 1;
    int pos             = 0;
    while (1) {
        if (dates[pos] == '\0') {
            break;
        }
        if (dates[pos] == ',') {
            ++intervals_count;
        }
        ++pos;
    }

    struct ast_event *cert_dates_event =
        ast_cert_dates_event_new(intervals_count);

    pos = 0;

    char *interval = strtok(dates, ",");

    while (interval) {
        int from = 0, to = 0;
        int readed = sscanf(interval, "%d-%d", &from, &to);
        if (readed != 2) {
            DBG("error during parse certified mothes. String: %s\n", dates);
            free(cert_dates_event);
            return;
        }
        cert_dates_event->event_data.dates.intervals[pos].start = from;
        cert_dates_event->event_data.dates.intervals[pos].end   = to;

        ++pos;
        interval = strtok(NULL, ",");
    }

    add_to_event_list(cert_dates_event);
}

static void on_certinfo_response(char *message_text)
{
    DBG("on_certinfo_response");

    uint32_t sell_date;
    int warranty;
    char limit[16]       = {};
    char credentials[32] = {};
    char cert_dates[512] = {};
    int params_read      = 0;
    if ((params_read =
             sscanf(message_text, "certinfo %u %d %15s %31s %511s", &sell_date,
                    &warranty, limit, credentials, cert_dates)) > 3) {
        DBG("certinfo %u %d %s %s", sell_date, warranty, limit, credentials);
        struct ast_event *cert_event = ast_event_new(AST_EVENT_CERTIFICATE);
        if (!cert_event) {
            state_error(AST_ERROR_CERTIFICATE_INFO);
            return;
        }

        cert_event->event_data.certificate = 0;

        if (strstr(credentials, "cdg")) {
            cert_event->event_data.certificate |= ast_cert_cdg;
        }
        if (strstr(credentials, "tcp")) {
            cert_event->event_data.certificate |= ast_cert_tcp;
        }

        add_to_event_list(cert_event);

        if (params_read == 5 && strlen(cert_dates) > 0) {
            process_cert_dates(cert_dates);
        } else {
            struct ast_event *cert_dates_event = ast_cert_dates_event_new(0);
            if (!cert_dates_event) {
                state_error(AST_ERROR_CERTIFICATE_INFO);
                return;
            }
            add_to_event_list(cert_dates_event);
        }

        sync_database();
    } else {
        state_error(AST_ERROR_CERTIFICATE_INFO);
    }
}

static void ask_certificate()
{
    DBG("ask_certificate");
    send_request("certinfo\r\n", (uv_stream_t *)api->tcp, 0);
}

int ast_ask_certificate()
{
    ask_certificate();
    return 0;
}

static void ask_effect_list();

static void free_effect_list()
{
    if (api->effect_count == 0) {
        return;
    }
    for (int i = 0; i < api->effect_count; i++) {
        free(api->effect_list[i].name);
        free(api->effect_list[i].abbriviation);
    }
    free(api->effect_list);
    api->effect_count = 0;
}

static void fill_effect_list_by_default()
{
    free_effect_list();
    api->effect_count =
        sizeof(mic_effects_default) / sizeof(struct mic_effect_data);
    api->effect_list =
        calloc(api->effect_count, sizeof(struct mic_effect_data));
    for (int i = 0; i < api->effect_count; i++) {
        api->effect_list[i].name = strdup(mic_effects_default[i].name);
        api->effect_list[i].abbriviation =
            strdup(mic_effects_default[i].abbriviation);
    }
    struct ast_event *event = ast_event_new(AST_EVENT_EFFECT_LIST_CHANGED);
    add_to_event_list(event);
}

static void on_features_response(char *resp_text)
{
    DBG("on_features_response: %s", resp_text);
    api->features = 0;
    if (strstr(resp_text, "volume-v2.1")) {
        api->features |= volume_v2;
    }
    if (strstr(resp_text, "effect-list")) {
        api->features |= effect_list;
    }
    if (strstr(resp_text, "pin-code-enabled")) {
        api->features |= pin_code_enabled;
    }
    if (api->features & effect_list) {
        ask_effect_list();
    } else {
        fill_effect_list_by_default();
        ask_certificate();
    }
}

static void ask_features()
{
    DBG("ask_features");
    send_request("featuresinfo\r\n", (uv_stream_t *)api->tcp, 0);
}

static void on_effect_list_response(char *text)
{
    DBG("on_effect_list_response %s\n", text);
    char *first_effect = text + strlen("effect-list") + 1;
    int list_len       = 0;
    char *temp;
    // calculate list length by calculating number of ':' in string
    for (temp = strchr(first_effect, ':'); temp; temp = strchr(temp + 1, ':')) {
        ++list_len;
    }

    free_effect_list();
    api->effect_list  = calloc(list_len, sizeof(struct mic_effect_data));
    api->effect_count = list_len;

    char *effect;
    char *sfptr1 = NULL;
    char *sfptr2 = NULL;
    char *name, *short_name;
    int i = 0;
    for (effect = strtok_r(first_effect, ";", &sfptr1); effect;
         effect = strtok_r(NULL, ";", &sfptr1)) {
        name                             = strtok_r(effect, ":", &sfptr2);
        short_name                       = strtok_r(NULL, ":", &sfptr2);
        api->effect_list[i].name         = strdup(name);
        api->effect_list[i].abbriviation = strdup(short_name);
        ++i;
    }
    struct ast_event *event = ast_event_new(AST_EVENT_EFFECT_LIST_CHANGED);
    add_to_event_list(event);
    if (api->state != ast_api_online) {
        ask_certificate();
    }
}

static void ask_effect_list()
{
    DBG("ask_effect_list");
    send_request("effect-list\r\n", (uv_stream_t *)api->tcp, 0);
}

static int fw_version_num(int major, int minor, int build)
{
    return (major * 100000) + (min(minor, 99) * 1000) + min(build, 999);
}

static void on_fw_info_response(char *message_text)
{
    DBG("on_fw_info_response");

    // fwinfo 455959840001 board/ast50 3.02.087 30001

    char sw_serial[13] = {};
    char board_id[13]  = {};
    int version_major  = 0;
    int version_minor  = 0;
    int build_number   = 0;
    int params_read    = 0;
    if ((params_read = sscanf(message_text, "fwinfo %12s %12s %d.%d.%d",
                              sw_serial, board_id, &version_major,
                              &version_minor, &build_number)) < 5) {
        DBG("unknown fwinfo response received: %s", message_text);
        state_error(AST_ERROR_FW_INFO);
        return;
    }

    if (g_minimumVersion != 0) {
        int min_ver = fw_version_num(g_minimumVersion->version_major,
                                     g_minimumVersion->version_minor,
                                     g_minimumVersion->build_number);
        int fw_ver = fw_version_num(version_major, version_minor, build_number);
        if (min_ver > fw_ver) {
            state_error(AST_ERROR_INCOMPATIBLE_FIRMWARE_VERSION);
            return;
        }
    }
    if (g_maximumVersion != 0) {
        int max_ver = fw_version_num(g_maximumVersion->version_major,
                                     g_maximumVersion->version_minor,
                                     g_maximumVersion->build_number);
        int fw_ver = fw_version_num(version_major, version_minor, build_number);
        if (fw_ver >= max_ver) {
            state_error(AST_ERROR_TOO_MODERN_FIRMWARE);
            return;
        }
    }

    struct ast_event *fwinfo_event = ast_event_new(AST_EVENT_FW_INFO);
    if (!fwinfo_event) {
        state_error(AST_ERROR_FW_INFO);
        return;
    }

    fwinfo_event->event_data.fw_info.version_major = version_major;
    fwinfo_event->event_data.fw_info.version_minor = version_minor;
    fwinfo_event->event_data.fw_info.build_number  = build_number;
    strncpy(fwinfo_event->event_data.fw_info.serial, sw_serial, 13);
    strncpy(fwinfo_event->event_data.fw_info.board_id, board_id, 13);

    DBG("fwinfo %d %d %d", version_major, version_minor, build_number);

    add_to_event_list(fwinfo_event);

    if (fw_version_num(version_major, version_minor, build_number) >=
        fw_version_num(4, 4, 0)) {
        ask_features();
    } else {
        api->features = 0;
        fill_effect_list_by_default();
        ask_certificate();
    }
}

static void on_fw_info_request(uv_write_t *req, int status)
{
    if (status < 0) {
        state_error(AST_ERROR_FW_INFO);
    } else {
        struct sockaddr_storage s = {0};
        int s_len                 = sizeof(s);
        int res = uv_tcp_getsockname(api->tcp, (struct sockaddr *)&s, &s_len);
        if (!res) {
            char receiver[16] = {0};
            res               = uv_ip4_name((struct sockaddr_in *)&s, receiver,
                              sizeof(receiver));
            if (res != 0) {
                DBG("uv_ip4_name error %d", res);
            } else {
                api->clientname = strdup(receiver);
            }
        } else {
            DBG("uv_tcp_getsockname error %d", res);
        }
        uv_read_start((uv_stream_t *)api->tcp, astapi_custom_alloc,
                      on_message_read);
    }

#ifdef _WIN32
    free(req->write_buffer.base);
#endif
    free(req);
}

static void ask_fw_info()
{
    DBG("ask_fw_info");
    send_request("fwinfo\r\n", (uv_stream_t *)api->tcp, on_fw_info_request);
}

int ast_fwinfo()
{
    DBG("ast_fwinfo");
    return send_msg("fwinfo\r\n");
}

static void on_connect(uv_connect_t *req, int status)
{
    DBG("on_connect %p status=%d", req, status);
    if (status < 0) {
        state_error(AST_ERROR_CONNECT);
    } else {
        ask_fw_info();
    }
}

static int send_request(const char *request_str, uv_stream_t *stream,
                        uv_write_cb cb)
{
    char *str = strdup(request_str);
    // buf.len does not account NULL-byte
    uv_buf_t buf    = uv_buf_init(str, strlen(str));
    uv_write_t *req = calloc(1, sizeof(*req));
    return uv_write(req, stream, &buf, 1, cb);
}

const char *ast_hostname()
{
    return api->hostname;
}

const char *ast_clientname()
{
    return api->clientname;
}

const char *media_voice(int voice)
{
    switch (voice) {
    case 1:
        return "m";
    case 2:
        return "f";
    case 3:
        return "d";
    default:
        assert(0 && "Invalid argument value. This is a logic error.");
        abort();
    }
}

static const char *minor_tones[] = {"Am",  "Bbm", "Bm", "Cm",  "C#m", "Dm",
                                    "D#m", "Em",  "Fm", "F#m", "Gm",  "G#m"};
static const char *major_tones[] = {"C",  "Db", "D",  "Eb", "E",  "F",
                                    "F#", "G",  "Ab", "A",  "Bb", "B"};

static const size_t MAX_TONES = sizeof(minor_tones) / sizeof(minor_tones[0]);

const char *key_name(int key)
{
    assert((key >= 0) && (key < 24));
    if (key < 12)
        return minor_tones[key % 12];

    return major_tones[key % 12];
}

int calculate_final_key(int key, int key_change)
{
    int d = (key < (int)MAX_TONES) ? 0 : MAX_TONES;
    int r = key + key_change;
    if (r >= 0)
        return d + (r % MAX_TONES);
    else
        return d + (max(min(MAX_TONES + r, MAX_TONES), 0));
}

int ast_format_key(int voice, int key, int key_change, char **res)
{
    assert(res);

    const int final_key      = calculate_final_key(key, key_change);
    int n                    = 0;
    const char *short_format = "%s%s";
    const char *long_format  = "%s%s (%+d)";
    if (key_change == 0) {
        n = snprintf(0, 0, short_format, media_voice(voice),
                     key_name(final_key));
    } else {
        n = snprintf(0, 0, long_format, media_voice(voice), key_name(final_key),
                     key_change);
    }

    n++;
    char *buf = malloc(n);
    if (!buf) {
        return -ENOMEM;
    }

    if (key_change == 0) {
        n = snprintf(buf, n, short_format, media_voice(voice),
                     key_name(final_key));
    } else {
        n = snprintf(buf, n, long_format, media_voice(voice),
                     key_name(final_key), key_change);
    }

    if (n <= 0) {
        // Something went utterly wrong
        free(buf);
        return -1;
    }

    *res = buf;
    return 0;
}

int calculate_final_tempo(int tempo, int tempo_change)
{
    return (int)round(tempo + tempo * tempo_change / 100.0);
}

int ast_format_tempo(int tempo, int tempo_change, char **res)
{
    assert(res);
    int n = 0;
    // tempo_change is in steps, -7..7, where 1 step == 4 percents
    tempo_change *= 4;
    const int final_tempo    = calculate_final_tempo(tempo, tempo_change);
    const char *short_format = "%d";
    const char *long_format  = "%d (%+d%%)";
    if (tempo_change == 0) {
        n = snprintf(0, 0, short_format, tempo);
    } else {
        n = snprintf(0, 0, long_format, final_tempo, tempo_change);
    }

    n++;
    char *buf = malloc(n);
    if (!buf) {
        return -ENOMEM;
    }

    if (tempo_change == 0) {
        n = snprintf(buf, n, short_format, tempo);
    } else {
        n = snprintf(buf, n, long_format, final_tempo, tempo_change);
    }

    if (n <= 0) {
        // Something went utterly wrong
        free(buf);
        return -1;
    }

    *res = buf;
    return 0;
}

static const char *MELODY_NAMES[] = {"Mute", "Low", "Normal", "Loud"};
static const size_t MELODY_NAMES_LENGTH =
    sizeof(MELODY_NAMES) / sizeof(MELODY_NAMES[0]);

// TODO: need to remove this and somehow provide include of astmediadb.h
#define MEDIA_TYPE_LIVE 1
#define MEDIA_TYPE_MIDI 2
#define MEDIA_TYPE_ANY (MEDIA_TYPE_LIVE | MEDIA_TYPE_MIDI)

int ast_format_melody(int type, int melody, char **res)
{
    assert((melody >= 0) && (melody < (int)MELODY_NAMES_LENGTH));
    assert(res);
    if ((type & MEDIA_TYPE_ANY) & MEDIA_TYPE_LIVE) {
        *res = strdup("N/A");
        return 0;
    }

    int n = snprintf(0, 0, "%s", MELODY_NAMES[melody]);
    n++;
    char *buf = malloc(n);
    if (!buf) {
        return -ENOMEM;
    }

    n = snprintf(buf, n, "%s", MELODY_NAMES[melody]);
    if (n <= 0) {
        // Something went utterly wrong
        free(buf);
        return -1;
    }

    *res = buf;
    return 0;
}

int ast_format_boardid(int board_id, char **res)
{
    switch (board_id) {
    case 80: {
        *res = strdup("AST-100");
        break;
    }
    case 25: {
        *res = strdup("AST Mini");
        break;
    }
    case 50: {
        *res = strdup("AST-50/Mini");
        break;
    }
    case 51: {
        *res = strdup("AST-50");
        break;
    }
    case 52: {
        *res = strdup("AST Home");
        break;
    }
    case 252: {
        *res = strdup("AST OneBox");
        break;
    }
    default: {
        int n = snprintf(0, 0, "AST-%d", board_id);
        n++;
        *res = malloc(n);
        if (*res == NULL) {
            return -ENOMEM;
        }
        n = snprintf(*res, n, "AST-%d", board_id);
        break;
    }
    }
    if (*res == NULL) {
        return -ENOMEM;
    }
    return 0;
}

int ast_parse_boardid(const char *s)
{
    if (!strncmpi(s, "AST", 3)) {
        s += 3;
    } else if (!strncmpi(s, "board/ast", 9)) {
        s += 9;
    } else {
        return -EINVAL;
    }

    // Skip whitespace/dash characters
    while (*s && (isspace(*s) || *s == '-')) {
        s++;
    }
    if (!*s) {
        return -EINVAL;
    }

    char *endptr;
    const long num = strtol(s, &endptr, 10);
    // If the string has been parsed as a number to the very end
    if (!*endptr) {
        if (num == 25 || num == 50 || num == 100 || num == 250) {
            return (int)num;
        }
        return -EINVAL;
    }

    // If some digits were met
    if (endptr != s) {
        // 25B, 50B
        // 50/Mini
        if (!strcmpi(endptr, "B") && (num == 25 || num == 50)) {
            return (int)num;
        } else if (!strcmpi(endptr, "/Mini") && (num == 50)) {
            return (int)num;
        }
        return -EINVAL;
    }

    if (!strcmpi(s, "Home")) {
        return 52;
    }
    if (!strcmpi(s, "OneBox")) {
        return 252;
    }
    return -EINVAL;
}
