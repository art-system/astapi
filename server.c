#include "server.h"
#include "crc32.h"
#include "miceffects.h"
#include "sds.h"
#include "stdint.h"
#include "sys/stat.h"

double next_timer_time()
{
    return mg_time() + 1.0;
}

void event_handler_udp(struct mg_connection *nc, int ev, void *ev_data)
{
    struct mbuf *io = &nc->recv_mbuf;

    uint32_t handshake_msg[15];
    handshake_msg[0]  = 0xAAACAAAB;
    handshake_msg[13] = 250;
    handshake_msg[14] = 0x30001;

    switch (ev) {
    case MG_EV_RECV:
        mg_send(nc, handshake_msg, 60);
        break;
    default:
        break;
    }
    mbuf_free(io);
}

void process_tcp_request(struct mbuf *income_buf,
                         struct mg_connection *connection,
                         struct ServerData *data);
void broadcast_state(struct mg_mgr *mgr);

void send_file(struct mg_connection *connection, const char *path);
void send_file_info(struct mg_connection *connection, const char *reply,
                    const char *path);
sds format_player_state(struct ServerData *server);

void reply_reserve(struct mg_connection *connection);
void add_to_reserve(struct ServerData *server, const char *cmd, bool priority);
void reserve_move(struct ServerData *server, const char *cmd);
void reserve_delete(struct ServerData *server, const char *cmd);
void reserve_clear(struct ServerData *server);

void start_playback(struct ServerData *server);
void pause_playback(struct ServerData *server);
void stop_playback(struct ServerData *server);
void progress_playback(struct ServerData *server);

void player_effect_modify(struct ServerData *server, const char *cmd);
void reply_player_effect(struct mg_connection *connection);

void mic_effect_modify(struct ServerData *server, const char *cmd);
void reply_mic_effect(struct mg_connection *connection);

void event_handler_tcp(struct mg_connection *nc, int ev, void *ev_data)
{
    struct mbuf *io = &nc->recv_mbuf;

    struct ServerData *data = nc->mgr->user_data;

    switch (ev) {
    case MG_EV_RECV: {
        process_tcp_request(io, nc, data);
        break;
    }
    case MG_EV_TIMER: {
        progress_playback(nc->mgr->user_data);
        broadcast_state(nc->mgr);
        mg_set_timer(nc, next_timer_time());
        break;
    }
    case MG_EV_ACCEPT: {
        struct ClientState *state   = malloc(sizeof(struct ClientState));
        state->subscribed           = false;
        nc->user_data               = state;
        data->reserve_changed       = true;
        data->mic_effect_changed    = true;
        data->player_effect_changed = true;
        break;
    }
    case MG_EV_CLOSE: {
        if (nc->user_data) {
            free(nc->user_data);
        }
        break;
    }
    default:
        break;
    }
    mbuf_free(io);
}

static bool command_matches(const char *command, const char *income)
{
    return strncmp(command, income, strlen(command)) == 0;
}

static const char *fwinfo_response = "fwinfo 658384003000 ast250 4.03.20\r\n";
static const char *certinfo_response =
    "certinfo 20180101 12 0 _ 201004-202004\r\n";

void process_tcp_request(struct mbuf *income_buf,
                         struct mg_connection *connection,
                         struct ServerData *data)
{
    struct ClientState *state = connection->user_data;
    if (state == NULL) {
        return;
    }
    const char *income = income_buf->buf;
    int commands_count;
    sds *commands =
        sdssplitlen(income, strlen(income), "\r\n", 2, &commands_count);

    for (int i = 0; i < commands_count; i++) {
        sds cmd = commands[i];
        if (command_matches("fwinfo", cmd)) {
            mg_send(connection, fwinfo_response, strlen(fwinfo_response));
        } else if (command_matches("certinfo", cmd)) {
            mg_send(connection, certinfo_response, strlen(certinfo_response));
        } else if (command_matches("dbinfo", cmd)) {
            send_file_info(connection, "dbinfo ", data->db_path);
        } else if (command_matches("dbsync", cmd)) {
            send_file(connection, data->db_path);
        } else if (command_matches("alterinfo", cmd)) {
            send_file_info(connection, "alterinfo ", data->alt_path);
        } else if (command_matches("altersync", cmd)) {
            send_file(connection, data->alt_path);
        } else if (command_matches("subscribe", cmd)) {
            state->subscribed = true;
            mg_send(connection, "ok\r\n", 4);
        } else if (command_matches("unsubscribe", cmd)) {
            state->subscribed = false;
            mg_send(connection, "ok\r\n", 4);
        } else if (command_matches("player-state", cmd)) {
            sds ps = format_player_state(connection->mgr->user_data);
            mg_send(connection, ps, sdslen(ps));
            sdsfree(ps);
        } else if (command_matches("reserve-list", cmd)) {
            reply_reserve(connection);
        } else if (command_matches("reserve-add", cmd)) {
            add_to_reserve(connection->mgr->user_data, cmd, false);
            reply_reserve(connection);
        } else if (command_matches("reserve-priority", cmd)) {
            add_to_reserve(connection->mgr->user_data, cmd, true);
            reply_reserve(
                connection); // NOTE: do not reply, wait for player-play
        } else if (command_matches("reserve-move", cmd)) {
            reserve_move(connection->mgr->user_data, cmd);
            reply_reserve(connection);
        } else if (command_matches("reserve-delete", cmd)) {
            reserve_delete(connection->mgr->user_data, cmd);
            reply_reserve(connection);
        } else if (command_matches("reserve-clear", cmd)) {
            reserve_clear(connection->mgr->user_data);
            reply_reserve(connection);
        } else if (command_matches("player-play", cmd)) {
            start_playback(connection->mgr->user_data);
            sds ps = format_player_state(connection->mgr->user_data);
            mg_send(connection, ps, sdslen(ps));
            sdsfree(ps);
        } else if (command_matches("player-pause", cmd)) {
            pause_playback(connection->mgr->user_data);
            sds ps = format_player_state(connection->mgr->user_data);
            mg_send(connection, ps, sdslen(ps));
            sdsfree(ps);
        } else if (command_matches("player-stop", cmd)) {
            stop_playback(connection->mgr->user_data);
            sds ps = format_player_state(connection->mgr->user_data);
            mg_send(connection, ps, sdslen(ps));
            sdsfree(ps);
        } else if (command_matches("player-modify", cmd) ||
                   (command_matches("volume", cmd))) {
            player_effect_modify(connection->mgr->user_data, cmd);
            reply_player_effect(connection);
        } else if (command_matches("mic-volume", cmd) ||
                   command_matches("mic-effect", cmd) ||
                   command_matches("mic-effect-level", cmd)) {
            mic_effect_modify(connection->mgr->user_data, cmd);
            reply_mic_effect(connection);
        }
    }
    sdsfreesplitres(commands, commands_count);
}

void send_file_info(struct mg_connection *connection, const char *reply,
                    const char *path)
{
    uint32_t crc = crc32_of_file(path);
    struct stat st;
    stat(path, &st);
    sds answer = sdsnew(reply);
    answer     = sdscatprintf(answer, "%x %lld\r\n", crc, st.st_size);
    mg_send(connection, answer, sdslen(answer));
    sdsfree(answer);
}

void send_file(struct mg_connection *connection, const char *path)
{
    FILE *fd = fopen(path, "r");
    if (fd == NULL) {
        return;
    }
    size_t readed;
    void *buf = malloc(64 * 1024);
    while ((readed = fread(buf, 1, 64 * 1024, fd))) {
        mg_send(connection, buf, readed);
    }
    fclose(fd);
}

sds format_reserve_state(struct ServerData *server);
void reply_reserve(struct mg_connection *connection)
{
    sds rs = format_reserve_state(connection->mgr->user_data);
    mg_send(connection, rs, sdslen(rs));
    sdsfree(rs);
}

void broadcast_state(struct mg_mgr *mgr)
{
    struct mg_connection *connection;
    struct ServerData *server_state = mgr->user_data;
    sds player_state                = format_player_state(mgr->user_data);
    bool clear_reserve_changed      = false;
    bool clear_mic_changed          = false;
    bool clear_player_changed       = false;
    for (connection = mg_next(mgr, NULL); connection != NULL;
         connection = mg_next(mgr, connection)) {
        struct ClientState *state = connection->user_data;
        if (!state || !state->subscribed) {
            continue;
        }
        mg_send(connection, player_state, sdslen(player_state));
        if (server_state->reserve_changed) {
            reply_reserve(connection);
            clear_reserve_changed = true;
        }
        if (server_state->player_effect_changed) {
            reply_player_effect(connection);
            clear_player_changed = true;
        }
        if (server_state->mic_effect_changed) {
            reply_mic_effect(connection);
            clear_mic_changed = true;
        }
    }
    sdsfree(player_state);
    if (clear_reserve_changed) {
        server_state->reserve_changed = false;
    }
    if (clear_player_changed) {
        server_state->player_effect_changed = false;
    }
    if (clear_mic_changed) {
        server_state->mic_effect_changed = false;
    }
}

sds format_player_state(struct ServerData *server)
{
    struct PlayerState *ps = &server->player_state;
    sds result             = sdsnew("player-state ");
    switch (ps->state) {
    case PlayerIdle:
        result = sdscatprintf(result, "idle");
        break;
    case PlayerPaused:
        result = sdscatprintf(result, "paused");
        break;
    default:
        result = sdscatprintf(result, "running");
        break;
    }
    result = sdscatprintf(result, " %d %d %d %d _ %d %d\r\n", ps->num, ps->tone,
                          ps->tempo, ps->melody, ps->position, ps->duration);
    return result;
}

sds format_reserve_state(struct ServerData *server)
{
    struct Reserve *reserve = server->reserve_state;
    sds result              = sdsnew("reserve-list");
    for (int i = 0; i < reserve->length; i++) {
        struct ReserveItem item = reserve->items[i];
        result = sdscatprintf(result, "\t%d %d %d %d _ _", item.num, item.tone,
                              item.tempo, item.melody);
    }
    result = sdscatprintf(result, "\r\n");
    return result;
}

static struct ReserveItem reserve_state_remove(struct Reserve *reserve, int pos)
{
    struct ReserveItem item = reserve->items[pos];
    for (int i = pos; i < reserve->length; i++) {
        reserve->items[i] = reserve->items[i + 1];
    }
    reserve->length -= 1;
    return item;
}

static void reserve_state_insert(struct Reserve *reserve,
                                 struct ReserveItem item, int pos)
{
    if (pos > reserve->length) {
        return;
    }
    for (int i = reserve->length; i > pos; i--) {
        reserve->items[i] = reserve->items[i - 1];
    }
    reserve->items[pos] = item;
    reserve->length += 1;
}

void add_to_reserve(struct ServerData *server, const char *cmd, bool priority)
{
    if (server->reserve_state->length == 20) {
        return;
    }
    int tone;
    int temp;
    int num;
    if (sscanf(cmd, "%*s %d %d %d %*s", &num, &tone, &temp) == 3) {
        struct ReserveItem newItem = {
            .num = num, .tempo = temp, .tone = tone, .melody = 0};
        if (!priority) {
            reserve_state_insert(server->reserve_state, newItem,
                                 server->reserve_state->length);
        } else {
            reserve_state_insert(server->reserve_state, newItem, 0);
        }
    }
}

void reserve_move(struct ServerData *server, const char *cmd)
{
    int from;
    int to;
    if (sscanf(cmd, "%*s %d %d %*s", &from, &to) == 2) {
        if (from == to) {
            return;
        }
        struct ReserveItem item =
            reserve_state_remove(server->reserve_state, from);
        reserve_state_insert(server->reserve_state, item, to);
    }
}

void reserve_delete(struct ServerData *server, const char *cmd)
{
    if (server->reserve_state->length == 0) {
        return;
    }
    int pos;
    if (sscanf(cmd, "%*s %d %*s", &pos) == 1) {
        reserve_state_remove(server->reserve_state, pos);
    }
}

void reserve_clear(struct ServerData *server)
{
    server->reserve_state->length = 0;
}

int reserve_alloc(struct Reserve **reserve)
{
    if (*reserve == NULL) {
        *reserve = malloc(sizeof(struct Reserve));
        if (*reserve != NULL) {
            (*reserve)->length = 0;
            return 0;
        }
    }
    return -ENOMEM;
}

void start_playback(struct ServerData *server)
{
    struct PlayerState *player = &server->player_state;
    if (player->state == PlayerRunning) {
        return;
    }
    if (player->state == PlayerPaused) {
        player->state = PlayerRunning;
        return;
    }
    struct Reserve *reserve = server->reserve_state;
    if (reserve->length == 0) {
        return;
    }
    struct ReserveItem song = reserve_state_remove(reserve, 0);
    server->reserve_changed = true;
    player->state           = PlayerRunning;
    player->num             = song.num;
    player->tempo           = song.tempo;
    player->tone            = song.tone;
    player->duration =
        150 + song.num % 9; // semi-rand, the same for the same song
    player->position = 0;
}

void pause_playback(struct ServerData *server)
{
    struct PlayerState *player = &server->player_state;
    if (player->state == PlayerPaused || player->state == PlayerIdle) {
        return;
    }
    player->state = PlayerPaused;
}

void stop_playback(struct ServerData *server)
{
    struct PlayerState *player = &server->player_state;
    player->state              = PlayerIdle;
    player->num                = 0;
    player->tempo              = 0;
    player->tone               = 0;
    player->position           = 0;
    player->duration           = 0;
}

void progress_playback(struct ServerData *server)
{
    struct PlayerState *player = &server->player_state;
    if (player->state == PlayerRunning) {
        if (player->position == player->duration) {
            stop_playback(server);
            start_playback(server);
        } else {
            player->position += 1;
        }
    }
}

void player_effect_modify(struct ServerData *server, const char *cmd)
{
    int value;
    if (sscanf(cmd, "%*s key %d", &value) == 1) {
        server->player_state.tone = value;
    } else if (sscanf(cmd, "%*s tempo %d", &value) == 1) {
        server->player_state.tempo = value;
    } else if (sscanf(cmd, "%*s melody %d", &value) == 1) {
        server->player_state.melody = value;
    } else if (sscanf(cmd, "volume %d", &value) == 1) {
        server->player_state.volume = value;
    }
}

void reply_player_effect(
    struct mg_connection *connection) // send usual player-state + volume
{
    struct ServerData *server = connection->mgr->user_data;
    sds rs                    = format_player_state(server);
    rs = sdscatprintf(rs, "volume %d\r\n", server->player_state.volume);
    mg_send(connection, rs, sdslen(rs));
    sdsfree(rs);
}

void mic_effect_modify(struct ServerData *server, const char *cmd)
{
    int value;
    char effect[10] = {0};
    if (sscanf(cmd, "mic-volume %d", &value) == 1) {
        server->mic_state.volume = value;
    } else if (sscanf(cmd, "mic-effect-level %d", &value) == 1) {
        server->mic_state.mic_effect_level = value;
    } else if (sscanf(cmd, "mic-effect %9s", effect) == 1) {
        strncpy(server->mic_state.mic_effect, effect, 10);
    }
}

void reply_mic_effect(struct mg_connection *connection)
{
    struct ServerData *server = connection->mgr->user_data;
    struct MicState *mic      = &server->mic_state;
    sds ms                    = sdsnew("mic-effect ");
    ms = sdscatprintf(ms, "%s %d %d\r\n", mic->mic_effect,
                      mic->mic_effect_level, mic->volume);
    mg_send(connection, ms, sdslen(ms));
    sdsfree(ms);
}
